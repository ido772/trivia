﻿using Client.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    class Settings
    {
        public static string LoggedUser = "";
        public static int ServerPort = 5555;
        public static IPAddress ServerAddress = IPAddress.Loopback;
        public static int RefreshTime = 3000;
        public static bool OnClose = false;
        public static bool IsInRoom = false;
        public static int TimerInterval = 100;
        public static bool IsInGame = false;
    }
}
