﻿using Client.Requests;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    public class Serializer
    {
        public static string SerializeRequest(Requests.IRequest request)
        {
            string result = "";

            // Add the request code to the serialized request string.
            result += (char)request.GetCode();

            // Add the content (length + content itself) to the serialized request string.
            string content = request.GetContent();
            result += content.Length.ToString("0000") + content;

            return result;
        }
    }
}
