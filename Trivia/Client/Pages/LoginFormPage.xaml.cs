﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Client.Pages
{
    /// <summary>
    /// Interaction logic for LoginFormPage.xaml
    /// </summary>
    public partial class LoginFormPage : Page
    {
        private Communicator m_communicator;

        public LoginFormPage(Communicator communicator)
        {
            InitializeComponent();

            this.m_communicator = communicator;
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Check if credentials are valid.
                if (CheckCredentials())
                {
                    // Try to log in.
                    string result = m_communicator.LogIn(GetUsername(), GetPassword());
                    Settings.LoggedUser = GetUsername();

                    // Switch current window's content to the menu page.
                    try
                    {
                        App.Current.MainWindow.Content = new MenuPage(m_communicator);
                    }
                    catch (Exception ex)
                    {
                        // Handle MenuPage initialization exceptions.
                        NavigationService.Navigate(new ErrorPage(ex.Message));
                    }
                }
            }
            catch (Exception ex)
            {
                txtError.Text = ex.Message;
            }
        }

        private void Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new SignupFormPage(m_communicator));
        }

        private string GetUsername()
        {
            return txtbxUsername.Text;
        }

        private string GetPassword()
        {
            return txtbxPassword.Password;
        }

        private bool CheckCredentials()
        {
            bool result = true;

            // Check for username validation.
            if (!CheckValidation(GetUsername()))
            {
                txtUsernameError.Visibility = Visibility.Visible;
                result = false;
            }
            else
            {
                txtUsernameError.Visibility = Visibility.Hidden;
            }

            // Check for password validation.
            if (!CheckValidation(GetPassword()))
            {
                txtPasswordError.Visibility = Visibility.Visible;
                result = false;
            }
            else
            {
                txtPasswordError.Visibility = Visibility.Hidden;
            }

            return result;
        }

        private bool CheckValidation(string credential)
        {
            // Check if credential is filled.
            if (string.IsNullOrEmpty(credential) || string.IsNullOrWhiteSpace(credential))
                return false;

            return true;
        }
    }
}
