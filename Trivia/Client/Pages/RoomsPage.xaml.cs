﻿using Client.DialogWindows;
using Client.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Client.Pages
{
    /// <summary>
    /// Interaction logic for RoomsPage.xaml
    /// </summary>
    public partial class RoomsPage : Page
    {
        private Communicator m_communicator;
        private Mutex m_communicationLock;
        private bool m_doRefresh;
        public List<Room> Rooms { get; private set; }

        public RoomsPage(Communicator communicator)
        {
            Settings.IsInRoom = false;
            InitializeComponent();

            m_communicator = communicator;
            m_doRefresh = true;
            m_communicationLock = new Mutex();

            KeepPageRefreshed();
        }

        private async Task KeepPageRefreshed()
        {
            while (m_doRefresh && !Settings.OnClose)
            {
                // Enter protected code area.
                m_communicationLock.WaitOne();

                // Refresh the page.
                if (!Settings.OnClose)
                    Refresh();

                // Leave protected code area.
                m_communicationLock.ReleaseMutex();

                // Wait until the next refreshment.
                await Task.Delay(Settings.RefreshTime);
            }
        }

        private void Refresh()
        {
            Rooms = m_communicator.GetRooms();
            lstRooms.ItemsSource = Rooms;
        }

        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var dialog = new CreateRoomDialogWindow(m_communicator);
                _ = dialog.ShowDialog();

                var newRoom = dialog.GetRoom();
                if (newRoom != null)
                {
                    m_doRefresh = false;
                    App.Current.MainWindow.Content = new RoomViewPage(m_communicator, newRoom, true);
                }
            }
            catch (Exception ex)
            {
                m_doRefresh = false;
                this.NavigationService.Navigate(new ErrorPage(ex.Message));
            }
        }

        private void btnJoin_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (lstRooms.SelectedItem != null)
                {
                    m_communicator.JoinRoom(lstRooms.SelectedItem as Room);
                    m_doRefresh = false;
                    App.Current.MainWindow.Content = new RoomViewPage(m_communicator, lstRooms.SelectedItem as Room, false);
                }
                else
                {
                    MessageBox.Show("Please select a room.");
                }
            }
            catch (Exception ex)
            {
                m_doRefresh = false;
                this.NavigationService.Navigate(new ErrorPage(ex.Message));
            }
        }

        private void lstRooms_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btnJoin.IsEnabled = lstRooms.SelectedItem != null && (lstRooms.SelectedItem as Room).isActive == 1;
        }
    }
}
