﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Client.Pages
{
    /// <summary>
    /// Interaction logic for SignupFormPage.xaml
    /// </summary>
    public partial class SignupFormPage : Page
    {
        private Communicator m_communicator;

        public SignupFormPage(Communicator communicator)
        {
            InitializeComponent();

            this.m_communicator = communicator;
        }

        private void btnSignup_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Check if credentials are valid.
                if (CheckCredentials())
                {
                    // Try to sign up.
                    string result = m_communicator.SignUp(GetUsername(), GetEmail(), GetPassword());

                    // After a successful signup, navigate to the login form page.
                    this.NavigationService.Navigate(new LoginFormPage(m_communicator));
                }
            }
            catch (Exception ex)
            {
                txtError.Text = ex.Message;
            }
        }

        private void Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.GoBack();
        }

        private string GetUsername()
        {
            return txtbxUsername.Text;
        }

        private string GetPassword()
        {
            return txtbxPassword.Password;
        }

        private string GetEmail()
        {
            return txtbxEmail.Text;
        }

        private bool CheckCredentials()
        {
            bool result = true;

            // Check for username validation.
            if (!CheckValidation(GetUsername()))
            {
                txtUsernameError.Visibility = Visibility.Visible;
                result = false;
            }
            else
            {
                txtUsernameError.Visibility = Visibility.Hidden;
            }

            // Check for password validation.
            if (!CheckValidation(GetPassword()))
            {
                txtPasswordError.Visibility = Visibility.Visible;
                result = false;
            }
            else
            {
                txtPasswordError.Visibility = Visibility.Hidden;
            }

            // Check for email validation.
            if (!CheckValidation(GetEmail()))
            {
                txtEmailError.Visibility = Visibility.Visible;
                result = false;
            }
            else
            {
                txtEmailError.Visibility = Visibility.Hidden;
            }

            return result;
        }

        private bool CheckValidation(string credential)
        {
            // Check if credential is filled.
            if (string.IsNullOrEmpty(credential) || string.IsNullOrWhiteSpace(credential))
                return false;

            return true;
        }
    }
}
