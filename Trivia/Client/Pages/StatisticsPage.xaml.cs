﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Client.Pages
{
    /// <summary>
    /// Interaction logic for StatisticsPage.xaml
    /// </summary>
    public partial class StatisticsPage : Page
    {
        private Communicator m_communicator;

        public StatisticsPage(Communicator communicator)
        {
            InitializeComponent();

            m_communicator = communicator;
            var stats = m_communicator.GetStatistics();
            ApplyTotalStats(stats);

        }

        public string PersonalStats;
        public List<Statistics> TotalStats;

        private void ApplyTotalStats(List<string> stats)
        {
            TotalStats = new List<Statistics>();

            foreach (string stat in stats)
            {
                var statInfo = stat.Split(':');
                string username = statInfo[0];
                string jsonString = "{" + statInfo[1].Replace('=', ':') + "}";

                Statistics statistics = JsonSerializer.Deserialize<Statistics>(jsonString);
                statistics.username = username;
                TotalStats.Add(statistics);
            }

            lstTotalStats.ItemsSource = TotalStats;
        }
    }

    public class Statistics
    {
        public string username { get; set; }
        public float average_answer_time { get; set; }
        public int correct_answers { get; set; }
        public int num_of_games { get; set; }
        public int total_answers { get; set; }

        public Statistics() { }

        public string AverageAnswerTime
        {
            get
            {
                return $"Average answer time: {average_answer_time:R}";
            }
        }
        public string AnswerRatio
        {
            get
            {
                return $"Correct answers: {correct_answers}/{total_answers} ({AnswerPercentage:G}%)";
            }
        }
        public string GamesPlayed
        {
            get
            {
                return $"Games played: {num_of_games} games";
            }
        }

        private float AnswerPercentage
        {
            get
            {
                float rate = (float)correct_answers / (float)total_answers;
                return rate * 100.0f;
            }
        }
    }
}
