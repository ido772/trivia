﻿using Client.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Client.Pages
{
    /// <summary>
    /// Interaction logic for MenuPage.xaml
    /// </summary>
    public partial class MenuPage : Page
    {
        private Communicator m_communicator;
        private RoomsPage roomsPage;
        private StatisticsPage statisticsPage;

        public MenuPage(Communicator communicator)
        {
            InitializeComponent();

            m_communicator = communicator;

            roomsPage = new RoomsPage(m_communicator);
            statisticsPage = new StatisticsPage(m_communicator);
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = tabs.Items.GetItemAt(tabs.SelectedIndex);

            if (item is TabItem)
            {
                TabItem tab = item as TabItem;
                string header = tab.Header.ToString();

                if (header == "Rooms")
                {
                    (tab.Content as Frame).NavigationService.Navigate(roomsPage);
                }
                else if (header == "Statistics")
                {
                    (tab.Content as Frame).NavigationService.Navigate(statisticsPage);
                }
                else if (header == "Details")
                {
                    // Do nothing for now.
                }
            }
        }
    }
}
