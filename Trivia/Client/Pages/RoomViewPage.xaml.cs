﻿using Client.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Client.Pages
{
    /// <summary>
    /// Interaction logic for RoomViewPage.xaml
    /// </summary>
    public partial class RoomViewPage : Page
    {
        private Communicator m_communicator;
        private Room m_metaData;
        private Mutex m_communicationLock;
        private bool m_isAdmin;
        private bool m_doRefresh;
        private bool m_hasGameStarted
        {
            get
            {
                return m_metaData.isActive == 2;
            }
        }
        private DispatcherTimer m_timer;
        private TimeSpan m_time;
        private int m_questionsPlayed;
        private Question m_currentQuestion;

        public RoomViewPage(Communicator communicator, Room metaData, bool isAdmin)
        {
            Settings.IsInRoom = true;
            InitializeComponent();

            m_communicationLock = new Mutex();

            m_communicator = communicator;
            m_metaData = metaData;
            m_isAdmin = isAdmin;
            m_doRefresh = true;
            m_questionsPlayed = 0;
            m_metaData.questionCount = 10;

            // Enable / Disable admin-only buttons.
            btnStart.IsEnabled = m_isAdmin;

            // Asynchronously start refreshing the page
            KeepPageRefreshed();
        }

        private void StartGame()
        {
            if (m_isAdmin)
            {
                m_communicator.StartGame();
            }

            btnStart.IsEnabled = false;
            m_metaData.isActive = 2;

            // Replace the gamestate with the game grid.
            txtGameState.Visibility = Visibility.Hidden;
            grdGame.Visibility = Visibility.Visible;

            DisplayNewQuestion();
        }

        private void DisplayNewQuestion()
        {
            if (m_questionsPlayed++ < m_metaData.questionCount)
            {
                // Get the next question from the server.
                m_currentQuestion = m_communicator.GetQuestion();

                // Set question.
                txtQuestion.Text = m_currentQuestion.QuestionLine;

                // Set answers.
                txtAnswer1.Text = m_currentQuestion.Answers[0];
                txtAnswer2.Text = m_currentQuestion.Answers[1];
                txtAnswer3.Text = m_currentQuestion.Answers[2];
                txtAnswer4.Text = m_currentQuestion.Answers[3];

                // Start answer timer.
                StartTimer(m_metaData.timePerQuestion);
            }
            else
            {
                m_doRefresh = true;
                WaitForGameResults();
            }
        }

        private void SubmitAnswer(string answer)
        {
            m_timer.Stop();

            try
            {
                // The correct answer's id will allways be 1.
                int answerId = 2;
                if (answer == m_currentQuestion.CorrectAnswer) answerId = 1;

                // Submit the answer.
                m_communicator.SubmitAnswer(answerId);

                // Display the next question (or results screen).
                DisplayNewQuestion();
            }
            catch (Exception e)
            {
                App.Current.MainWindow.Content = new ErrorPage(e.Message);
            }
        }

        private void StartTimer(int seconds)
        {
            m_time = TimeSpan.FromSeconds(seconds);

            m_timer = new DispatcherTimer(TimeSpan.FromMilliseconds(Settings.TimerInterval), DispatcherPriority.Normal, delegate
            {
                txtTimeLeft.Text = m_time.Seconds + " : " + m_time.Milliseconds / Settings.TimerInterval;
                if (m_time == TimeSpan.Zero)
                {
                    // When time runs out, submit a wrong answer.
                    m_timer.Stop();
                    SubmitAnswer("This code is really bad and I know it :)");
                    DisplayNewQuestion();
                }
                m_time = m_time.Add(TimeSpan.FromMilliseconds(-Settings.TimerInterval));
            }, Application.Current.Dispatcher);

            m_timer.Start();
        }

        private async Task KeepPageRefreshed()
        {
            while (m_doRefresh && !Settings.OnClose && !m_hasGameStarted)
            {
                // Enter protected code area.
                m_communicationLock.WaitOne();

                // Refresh the room.
                if (!Settings.OnClose && !m_hasGameStarted)
                    Refresh();

                // Leave protected code area.
                m_communicationLock.ReleaseMutex();

                // Wait until the next refreshment.
                await Task.Delay(Settings.RefreshTime);
            }

            if (m_hasGameStarted)
            {
                StartGame();
            }
        }

        private void Refresh()
        {
            try
            {
                // Get the room's state.
                var state = m_communicator.GetRoomState();

                // Update the players list.
                lstPlayers.ItemsSource = state.players;

                // Update metadata.
                m_metaData.isActive = state.hasGameBegun ? 2 : 1;
            }
            catch (Exception e)
            {
                if (e.Message == "Room is no longer available")
                {
                    Window win = new Window();
                    win.Height = 250;
                    win.Width = 400;
                    win.Content = new ErrorPage(e.Message);
                    win.ShowDialog();

                    m_doRefresh = false;
                    App.Current.MainWindow.Content = new MenuPage(m_communicator);
                }
                else
                {
                    m_doRefresh = false;
                    App.Current.MainWindow.Content = new ErrorPage(e.Message);
                }
            }
        }

        private async Task WaitForGameResults()
        {
            // Wait for the game to end and get the results.
            GetGameResultsResponse gameResults;
            do
            {
                gameResults = m_communicator.GetGameResults();

                // Hide the game grid.
                grdGame.Visibility = Visibility.Hidden;

                // Display the game state screen, waiting for the game to end.
                txtGameState.Text = "Waiting for other players to finish.";
                txtGameState.Visibility = Visibility.Visible;

                // Wait until next refresh.
                await Task.Delay(Settings.RefreshTime / 3);
            }
            while (!Settings.OnClose && (gameResults.status == 0) && m_doRefresh);

            // Display the results.
            DisplayGameResults(gameResults.results);
        }

        private void DisplayGameResults(List<PlayerResults> results)
        {
            // Show the results grid.
            txtGameState.Visibility = Visibility.Hidden;
            grdResults.Visibility = Visibility.Visible;

            lstResults.ItemsSource = results;
        }

        private void btnLeave_Click(object sender, RoutedEventArgs e)
        {
            // Enter protected area.
            m_communicationLock.WaitOne();

            if (m_hasGameStarted)
            {
                m_communicator.LeaveGame();
            }
            else if (m_isAdmin)
            {
                m_communicator.CloseRoom();
            }
            else
            {
                m_communicator.LeaveRoom();
            }
            App.Current.MainWindow.Content = new MenuPage(m_communicator);

            m_doRefresh = false;

            // Leave protected area.
            m_communicationLock.ReleaseMutex();
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            // Enter protected area.
            m_communicationLock.WaitOne();

            StartGame();

            // Leave protected area.
            m_communicationLock.ReleaseMutex();
        }

        private void btnAnswer1_Click(object sender, RoutedEventArgs e)
        {
            SubmitAnswer(txtAnswer1.Text);
        }

        private void btnAnswer2_Click(object sender, RoutedEventArgs e)
        {
            SubmitAnswer(txtAnswer2.Text);
        }

        private void btnAnswer3_Click(object sender, RoutedEventArgs e)
        {
            SubmitAnswer(txtAnswer3.Text);
        }

        private void btnAnswer4_Click(object sender, RoutedEventArgs e)
        {
            SubmitAnswer(txtAnswer4.Text);
        }
    }
}
