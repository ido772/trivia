﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace Client.Responses
{
    public class GeneralResponse : IResponse
    {
        public int status { get; set; }

        public GeneralResponse() { }
        public GeneralResponse(int status)
        {
            this.status = status;
        }
    }
}
