﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Responses
{
    public class GetStatisticsResponse : IResponse
    {
        public List<string> statistics { get; set; }

        public GetStatisticsResponse() { }
    }
}
