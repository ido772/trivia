﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Responses
{
    class ErrorResponse : IResponse
    {
        public string message { get; set; }

        public ErrorResponse() { }

        public ErrorResponse(string error)
        {
            message = error;
        }
    }
}
