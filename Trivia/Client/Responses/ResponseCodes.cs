﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Responses
{
    public class ResponseCodes
    {
		public const byte ErrorResponseCode = 100;
		public const byte LoginResponseCode = 2;
		public const byte SignupResponseCode = 4;
		public const byte LogoutResponseCode = 6;
		public const byte GetRoomsResponseCode = 8;
		public const byte GetPlayersInRoomResponseCode = 10;
		public const byte GetStatisticsResponseCode = 12;
		public const byte JoinRoomResponseCode = 14;
		public const byte CreateRoomResponseCode = 16;
		public const byte CloseRoomResponseCode = 18;
		public const byte StartGameResponseCode = 20;
		public const byte GetRoomStateResponseCode = 22;
		public const byte LeaveRoomResponseCode = 24;
		public const byte LeaveGameResponseCode = 26;
		public const byte SubmitAnswerResponseCode = 28;
		public const byte GetQuestionResponseCode = 30;
		public const byte GetGameResultsResponseCode = 32;
	}
}
