﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Responses
{
    public class GetGameResultsResponse : IResponse
    {
        public int status { get; set; }
        public List<PlayerResults> results { get; set; }

        public GetGameResultsResponse()
        {
        }
    }

    public class PlayerResults
    {
        public string username { get; set; }
        public int correctAnswerCount { get; set; }
        public int wrongAnswerCount { get; set; }
        public double averageAnswerTime { get; set; }

        public PlayerResults()
        {
        }
    }
}
