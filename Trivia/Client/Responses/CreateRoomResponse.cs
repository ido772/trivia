﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Responses
{
    public class CreateRoomResponse : IResponse
    {
        public int status { get; set; }
        public int roomId { get; set; }

        public CreateRoomResponse()
        {

        }
    }
}
