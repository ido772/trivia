﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Client.Responses
{
    public class GetRoomsResponse : IResponse
    {
        public int status { get; set; }
        public List<Room> rooms { get; set; }

        public GetRoomsResponse()
        {
        }
    }

    public class Room
    {
        public int id { get; set; }
        public int isActive { get; set; }
        public int maxPlayers { get; set; }
        public string name { get; set; }
        public int timePerQuestion { get; set; }
        public int questionCount { get; set; }

        public Room()
        {
        }

        public Brush ActiveColor
        {
            get
            {
                if (isActive == 1) return Brushes.LimeGreen;
                else if (isActive == 2) return Brushes.DodgerBlue;
                return Brushes.Red;
            }
        }
    }
}
