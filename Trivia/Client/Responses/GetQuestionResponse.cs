﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Responses
{
    public class GetQuestionResponse : IResponse
    {
        public int status { get; set; }
        public string question { get; set; }
        public List<string> answers { get; set; }

        public GetQuestionResponse()
        {
        }
    }

    public class Question
    {
        public string QuestionLine { get; }
        public string CorrectAnswer { get; }
        public List<string> Answers { get; }

        public Question(string question, List<string> answers)
        {
            QuestionLine = question;

            // The first answer will allways be the correct one.
            CorrectAnswer = answers[0];

            Answers = answers;
            Shuffle(Answers);
        }

        public static void Shuffle(List<string> list)
        {
            Random rnd = new Random();
            for (int i = 0; i < list.Count - 1; ++i)
            {
                // Randomize the order of the list.
                int r = rnd.Next(i, list.Count);
                string tmp = list[i];
                list[i] = list[r];
                list[r] = tmp;
            }
        }
    }
}
