﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Responses
{
    public class GetRoomStateResponse : IResponse
    {
        public int status { get; set; }
        public bool hasGameBegun { get; set; }
        public List<string> players { get; set; }
        public int questionCount { get; set; }
        public double answerTimeout { get; set; }

        public GetRoomStateResponse()
        {
        }
    }
}
