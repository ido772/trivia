﻿using Client.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Markup;

namespace Client
{
    public class Deserializer
    {
        public static IResponse DeserializeResponse(string response)
        {
            byte code = Encoding.ASCII.GetBytes(response)[0];

            string jsonString = response.Substring(5);

            switch (code)
            {
                case ResponseCodes.ErrorResponseCode:
                    return DeserializeErrorResponse(jsonString);
                case ResponseCodes.SignupResponseCode:
                    return DeserializeGeneralResponse(jsonString);
                case ResponseCodes.LoginResponseCode:
                    return DeserializeGeneralResponse(jsonString);
                case ResponseCodes.LogoutResponseCode:
                    return DeserializeGeneralResponse(jsonString);
                case ResponseCodes.GetRoomsResponseCode:
                    return DeserializeGetRoomsResponse(jsonString);
                case ResponseCodes.CreateRoomResponseCode:
                    return DeserializeCreateRoomResponse(jsonString);
                case ResponseCodes.GetStatisticsResponseCode:
                    return DeserializeGetStatisticsResponse(jsonString);
                case ResponseCodes.JoinRoomResponseCode:
                    return DeserializeGeneralResponse(jsonString);
                case ResponseCodes.CloseRoomResponseCode:
                    return DeserializeGeneralResponse(jsonString);
                case ResponseCodes.StartGameResponseCode:
                    return DeserializeGeneralResponse(jsonString);
                case ResponseCodes.GetRoomStateResponseCode:
                    return DeserializeGetRoomStateResponse(jsonString);
                case ResponseCodes.LeaveRoomResponseCode:
                    return DeserializeGeneralResponse(jsonString);
                case ResponseCodes.LeaveGameResponseCode:
                    return DeserializeGeneralResponse(jsonString);
                case ResponseCodes.GetQuestionResponseCode:
                    return DeserializeGetQuestionResponse(jsonString);
                case ResponseCodes.SubmitAnswerResponseCode:
                    return DeserializeGeneralResponse(jsonString);
                case ResponseCodes.GetGameResultsResponseCode:
                    return DeserializeGetGameResultsResponse(jsonString);
                default:
                    return DeserializeErrorResponse("{\"message\": \"Unknown response\"}");
            }
        }

        private static GeneralResponse DeserializeGeneralResponse(string jsonString)
        {
            GeneralResponse response = JsonSerializer.Deserialize<GeneralResponse>(jsonString);
            return response;
        }

        private static ErrorResponse DeserializeErrorResponse(string jsonString)
        {
            ErrorResponse response = JsonSerializer.Deserialize<ErrorResponse>(jsonString);
            return response;
        }

        private static GetRoomsResponse DeserializeGetRoomsResponse(string jsonString)
        {
            GetRoomsResponse response = JsonSerializer.Deserialize<GetRoomsResponse>(jsonString);
            return response;
        }

        public static CreateRoomResponse DeserializeCreateRoomResponse(string jsonString)
        {
            CreateRoomResponse response = JsonSerializer.Deserialize<CreateRoomResponse>(jsonString);
            return response;
        }

        private static GetStatisticsResponse DeserializeGetStatisticsResponse(string jsonString)
        {
            GetStatisticsResponse response = JsonSerializer.Deserialize<GetStatisticsResponse>(jsonString);
            return response;
        }

        private static GetRoomStateResponse DeserializeGetRoomStateResponse(string jsonString)
        {
            GetRoomStateResponse response = JsonSerializer.Deserialize<GetRoomStateResponse>(jsonString);
            return response;
        }

        private static GetQuestionResponse DeserializeGetQuestionResponse(string jsonString)
        {
            GetQuestionResponse response = JsonSerializer.Deserialize<GetQuestionResponse>(jsonString);
            return response;
        }

        private static GetGameResultsResponse DeserializeGetGameResultsResponse(string jsonString)
        {
            GetGameResultsResponse response = JsonSerializer.Deserialize<GetGameResultsResponse>(jsonString);
            return response;
        }
    }
}
