﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;

namespace Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class TestingWindow : Window
    {
        private Communicator m_communicator;

        public TestingWindow()
        {
            InitializeComponent();

            try
            {
                m_communicator = new Communicator();
            }
            catch (Exception e)
            {
                txtResponse.Text = "Error: " + e.Message;
            }
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            txtResponse.Text = m_communicator.LogIn(txtbxLoginUser.Text, txtbxLoginPassword.Text);
        }

        private void btnSignup_Click(object sender, RoutedEventArgs e)
        {
            txtResponse.Text = m_communicator.SignUp(txtbxSignupUser.Text, txtbxSignupEmail.Text, txtbxSignupPassword.Text);
        }
    }
}
