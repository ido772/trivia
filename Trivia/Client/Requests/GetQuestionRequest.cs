﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Requests
{
    public class GetQuestionRequest : IRequest
    {
        public GetQuestionRequest()
        {
        }

        public byte GetCode()
        {
            return RequestCodes.GetQuestionRequestCode;
        }

        public string GetContent()
        {
            string jsonContent = "{}";
            return jsonContent;
        }
    }
}
