﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Requests
{
    public class GetRoomsRequest : IRequest
    {
        public GetRoomsRequest()
        {
        }

        public byte GetCode()
        {
            return RequestCodes.GetRoomsRequestCode;
        }

        public string GetContent()
        {
            string jsonContent = "{}";
            return jsonContent;
        }
    }
}
