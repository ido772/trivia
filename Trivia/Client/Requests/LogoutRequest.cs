﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Requests
{
    public class LogoutRequest : IRequest
    {
        public LogoutRequest()
        {
        }

        public byte GetCode()
        {
            return RequestCodes.LogoutRequestCode;
        }

        public string GetContent()
        {
            string jsonContent = "{}";
            return jsonContent;
        }
    }
}
