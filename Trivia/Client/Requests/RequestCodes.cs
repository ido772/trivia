﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace Client.Requests
{
    public class RequestCodes
    {
        public const byte SignupRequestCode = 1;
        public const byte LoginRequestCode = 3;
		public const byte LogoutRequestCode = 5;
		public const byte GetRoomsRequestCode = 7;
		public const byte GetPlayersInRoomRequestCode = 9;
		public const byte GetStatisticsRequestCode = 11;
		public const byte JoinRoomRequestCode = 13;
		public const byte CreateRoomRequestCode = 15;
		public const byte CloseRoomRequestCode = 17;
		public const byte StartGameRequestCode = 19;
		public const byte GetRoomStateRequestCode = 21;
		public const byte LeaveRoomRequestCode = 23;
		public const byte LeaveGameRequestCode = 25;
		public const byte GetQuestionRequestCode = 27;
		public const byte SubmitAnswerRequestCode = 29;
		public const byte GetGameResultsRequestCode = 31;
    }
}
