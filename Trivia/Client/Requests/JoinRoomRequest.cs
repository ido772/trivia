﻿using Client.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Requests
{
    public class JoinRoomRequest : IRequest
    {
        private Room metaData;

        public JoinRoomRequest(Room room)
        {
            metaData = room;
        }

        public byte GetCode()
        {
            return RequestCodes.JoinRoomRequestCode;
        }

        public string GetContent()
        {
            string jsonString = "{";
            jsonString += $"\"roomId\": {metaData.id}";
            jsonString += "}";

            return jsonString;
        }
    }
}
