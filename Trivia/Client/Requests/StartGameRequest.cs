﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Requests
{
    public class StartGameRequest : IRequest
    {
        public StartGameRequest()
        {
        }

        public byte GetCode()
        {
            return RequestCodes.StartGameRequestCode;
        }

        public string GetContent()
        {
            string jsonString = "{}";
            return jsonString;
        }
    }
}
