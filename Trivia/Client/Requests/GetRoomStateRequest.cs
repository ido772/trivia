﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Requests
{
    public class GetRoomStateRequest : IRequest
    {
        public GetRoomStateRequest()
        {
        }

        public byte GetCode()
        {
            return RequestCodes.GetRoomStateRequestCode;
        }

        public string GetContent()
        {
            string jsonString = "{}";
            return jsonString;
        }
    }
}
