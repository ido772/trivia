﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Requests
{
    class CreateRoomRequest : IRequest
    {
        private string m_sName;
        private int m_uiMaxPlayers;
        private int m_uiQuestionsCount;
        private int m_uiAnswerTimeout;

        public CreateRoomRequest(string name, int maxPlayers, int questionsCount, int answerTimeout)
        {
            m_sName = name;
            m_uiMaxPlayers = maxPlayers;
            m_uiQuestionsCount = questionsCount;
            m_uiAnswerTimeout = answerTimeout;
        }

        public byte GetCode()
        {
            return RequestCodes.CreateRoomRequestCode;
        }

        public string GetContent()
        {
            string jsonString = "{";
            jsonString += $"\"name\": \"{m_sName}\", \"maxPlayers\": {m_uiMaxPlayers}, ";
            jsonString += $"\"questionsCount\": {m_uiQuestionsCount}, \"answerTimeout\": {m_uiAnswerTimeout}";
            jsonString += "}";

            return jsonString;
        }
    }
}
