﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Requests
{
    public class SignupRequest : IRequest
    {
        private string m_sUsername;
        private string m_sPassword;
        private string m_sEmail;

        public SignupRequest(string username, string password, string email)
        {
            this.m_sUsername = username;
            this.m_sPassword = password;
            this.m_sEmail = email;
        }

        public byte GetCode()
        {
            return RequestCodes.SignupRequestCode;
        }

        public string GetContent()
        {
            string jsonContent = "{";
            jsonContent += $"\"username\": \"{m_sUsername}\", \"password\": \"{m_sPassword}\", \"email\": \"{m_sEmail}\"";
            jsonContent += "}";

            return jsonContent;
        }
    }
}
