﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Requests
{
    public class GetStatisticsRequest : IRequest
    {
        public GetStatisticsRequest() { }

        public byte GetCode()
        {
            return RequestCodes.GetStatisticsRequestCode;
        }

        public string GetContent()
        {
            return "{}";
        }
    }
}
