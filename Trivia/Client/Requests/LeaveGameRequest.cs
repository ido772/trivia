﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Requests
{
    public class LeaveGameRequest : IRequest
    {
        public LeaveGameRequest()
        {
        }

        public byte GetCode()
        {
            return RequestCodes.LeaveGameRequestCode;
        }

        public string GetContent()
        {
            string jsonContent = "{}";
            return jsonContent;
        }
    }
}
