﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Requests
{
    public class LeaveRoomRequest : IRequest
    {
        public LeaveRoomRequest()
        {
        }

        public byte GetCode()
        {
            return RequestCodes.LeaveRoomRequestCode;
        }

        public string GetContent()
        {
            string jsonContent = "{}";
            return jsonContent;
        }
    }
}
