﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Requests
{
    public class LoginRequest : IRequest
    {
        private string m_sUsername;
        private string m_sPassword;

        public LoginRequest(string username, string password)
        {
            this.m_sUsername = username;
            this.m_sPassword = password;
        }

        public byte GetCode()
        {
            return RequestCodes.LoginRequestCode;
        }

        public string GetContent()
        {
            string jsonContent = "{";
            jsonContent += $"\"username\": \"{m_sUsername}\", \"password\": \"{m_sPassword}\"";
            jsonContent += "}";

            return jsonContent;
        }
    }
}
