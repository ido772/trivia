﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Requests
{
    public class CloseRoomRequest : IRequest
    {
        public CloseRoomRequest()
        {
        }

        public byte GetCode()
        {
            return RequestCodes.CloseRoomRequestCode;
        }

        public string GetContent()
        {
            string jsonString = "{}";
            return jsonString;
        }
    }
}
