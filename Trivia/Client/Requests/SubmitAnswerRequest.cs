﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Requests
{
    public class SubmitAnswerRequest : IRequest
    {
        private int answerId;

        public SubmitAnswerRequest(int answerId)
        {
            this.answerId = answerId;
        }

        public byte GetCode()
        {
            return RequestCodes.SubmitAnswerRequestCode;
        }

        public string GetContent()
        {
            string jsonContent = "{";
            jsonContent += $"\"answerId\": {answerId}";
            jsonContent += "}";

            return jsonContent;
        }
    }
}
