﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client.Requests
{
    public class GetGameResultsRequest : IRequest
    {
        public GetGameResultsRequest()
        {
        }

        public byte GetCode()
        {
            return RequestCodes.GetGameResultsRequestCode;
        }

        public string GetContent()
        {
            string jsonContent = "{}";
            return jsonContent;
        }
    }
}
