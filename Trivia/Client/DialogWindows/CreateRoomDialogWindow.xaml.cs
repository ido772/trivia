﻿using Client.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Client.DialogWindows
{
    /// <summary>
    /// Interaction logic for CreateRoomDialogWindow.xaml
    /// </summary>
    public partial class CreateRoomDialogWindow : Window
    {
        private Communicator m_communicator;
        private Room m_room;

        public CreateRoomDialogWindow(Communicator communicator)
        {
            InitializeComponent();

            m_communicator = communicator;
            m_room = null;
        }

        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            int id = m_communicator.CreateRoom(
                txtName.Text,
                (int)sldPlayers.Value,
                10,
                (int)sldAnswerTimeout.Value
                );

            m_room = new Room();
            m_room.id = id;
            m_room.isActive = 1;
            m_room.name = txtName.Text;
            m_room.maxPlayers = (int)sldPlayers.Value;
            m_room.timePerQuestion = (int)sldAnswerTimeout.Value;
            m_room.questionCount = 10;

            this.Close();
        }

        public Room GetRoom() { return m_room; }
    }
}
