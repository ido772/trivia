﻿using Client.Responses;
using Client.Requests;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Documents;
using System.Collections.Generic;

namespace Client
{
    public class Communicator
    {
        private Socket connection;

        public Communicator()
        {
            OpenConnection();
        }



        public string SignUp(string username, string email, string password)
        {
            SignupRequest request = new SignupRequest(username, password, email);
            return HandleGeneralRequest(request);
        }

        public string LogIn(string username, string password)
        {
            LoginRequest request = new LoginRequest(username, password);
            return HandleGeneralRequest(request);
        }

        public string LogOut()
        {
            IRequest request;

            if (Settings.IsInRoom)
            {
                request = new LeaveRoomRequest();
                _ = HandleGeneralRequest(request);
            }
            request = new LogoutRequest();

            return HandleGeneralRequest(request);
        }

        public List<Room> GetRooms()
        {
            GetRoomsRequest request = new GetRoomsRequest();
            return HandleGetRoomsRequest(request);
        }

        public int CreateRoom(string name, int maxPlayers, int questionsCount, int answerTimeout)
        {
            CreateRoomRequest request = new CreateRoomRequest(name, maxPlayers, questionsCount, answerTimeout);
            return HandleCreateRoomRequest(request);
        }

        public List<string> GetStatistics()
        {
            GetStatisticsRequest request = new GetStatisticsRequest();
            return HandleGetStatisticsRequest(request);
        }

        public string JoinRoom(Room room)
        {
            JoinRoomRequest request = new JoinRoomRequest(room);
            return HandleGeneralRequest(request);
        }

        public string CloseRoom()
        {
            CloseRoomRequest request = new CloseRoomRequest();
            return HandleGeneralRequest(request);
        }

        public string StartGame()
        {
            StartGameRequest request = new StartGameRequest();
            return HandleGeneralRequest(request);
        }

        public GetRoomStateResponse GetRoomState()
        {
            GetRoomStateRequest request = new GetRoomStateRequest();
            return HandleGetRoomStateRequest(request);
        }

        public string LeaveRoom()
        {
            if (Settings.IsInGame)
            {
                return LeaveGame();
            }

            LeaveRoomRequest request = new LeaveRoomRequest();
            return HandleGeneralRequest(request);
        }

        public Question GetQuestion()
        {
            GetQuestionRequest request = new GetQuestionRequest();
            return HandleGetQuestionRequest(request);
        }

        public string LeaveGame()
        {
            LeaveGameRequest request = new LeaveGameRequest();
            return HandleGeneralRequest(request);
        }

        public string SubmitAnswer(int answerId)
        {
            SubmitAnswerRequest request = new SubmitAnswerRequest(answerId);
            return HandleGeneralRequest(request);
        }

        public GetGameResultsResponse GetGameResults()
        {
            GetGameResultsRequest request = new GetGameResultsRequest();
            return HandleGetGameResultsRequest(request);
        }



        private void OpenConnection()
        {
            try
            {
                // Establish the remote endpoint for the socket.
                // Set the endpoint to local and use the server's port.
                IPEndPoint remoteEP = new IPEndPoint(Settings.ServerAddress, Settings.ServerPort);

                // Create a TCP/IP  socket.  
                this.connection = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                // Connect the socket to the remote endpoint. 
                try
                {
                    connection.Connect(remoteEP);

                    Console.WriteLine("Client is connected to {0}",
                        connection.RemoteEndPoint.ToString());
                }
                catch (ArgumentNullException ane)
                {
                    throw new Exception("ArgumentNullException:\n" + ane.Message);
                }
                catch (SocketException se)
                {
                    throw new Exception("SocketException:\n" + se.Message);
                }
            }
            catch (Exception e)
            {
                throw new Exception("Connection error:\n" + e.Message);
            }
        }

        private void CloseConnection()
        {
            try
            {
                if (connection.Connected)
                {
                    // Release the socket.  
                    connection.Shutdown(SocketShutdown.Both);
                    connection.Close();
                }
            }
            catch (ArgumentNullException ane)
            {
                Console.WriteLine("ArgumentNullException : {0}", ane.ToString());
            }
            catch (SocketException se)
            {
                Console.WriteLine("SocketException : {0}", se.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine("Unexpected exception : {0}", e.ToString());
            }
        }

        private void SendRequest(IRequest request)
        {
            // Encode the data string into a byte array.  
            byte[] msg = Encoding.ASCII.GetBytes(Serializer.SerializeRequest(request));

            // Send the data through the socket.  
            int bytesSent = connection.Send(msg);
        }

        private IResponse ReceiveResponse()
        {
            byte[] buffer = new byte[1024];

            // Receive the response from the remote device.  
            int bytesRec = connection.Receive(buffer);
            string response = Encoding.ASCII.GetString(buffer, 0, bytesRec);

            // Deserialize the response.
            IResponse deserializedResponse = Deserializer.DeserializeResponse(response);
            return deserializedResponse;
        }

        private string HandleGeneralRequest(IRequest request)
        {
            SendRequest(request);
            IResponse response = ReceiveResponse();

            if (response is ErrorResponse)
            {
                throw new Exception((response as ErrorResponse).message);
            }
            else if (response is GeneralResponse)
            {
                return (response as GeneralResponse).status.ToString();
            }
            else
            {
                return "?";
            }
        }

        private List<Room> HandleGetRoomsRequest(GetRoomsRequest request)
        {
            SendRequest(request);
            IResponse response = ReceiveResponse();

            if (response is ErrorResponse)
            {
                throw new Exception((response as ErrorResponse).message);
            }
            else if (response is GetRoomsResponse)
            {
                return (response as GetRoomsResponse).rooms;
            }
            else
            {
                return new List<Room>();
            }
        }

        private List<string> HandleGetStatisticsRequest(GetStatisticsRequest request)
        {
            SendRequest(request);
            IResponse response = ReceiveResponse();

            if (response is ErrorResponse)
            {
                throw new Exception((response as ErrorResponse).message);
            }
            else if (response is GetStatisticsResponse)
            {
                return (response as GetStatisticsResponse).statistics;
            }
            else
            {
                return new List<string>();
            }
        }

        private int HandleCreateRoomRequest(CreateRoomRequest request)
        {
            SendRequest(request);
            IResponse response = ReceiveResponse();

            if (response is ErrorResponse)
            {
                throw new Exception((response as ErrorResponse).message);
            }
            else if (response is CreateRoomResponse)
            {
                return (response as CreateRoomResponse).roomId;
            }
            else
            {
                return -1;
            }
        }

        private GetRoomStateResponse HandleGetRoomStateRequest(GetRoomStateRequest request)
        {
            SendRequest(request);
            IResponse response = ReceiveResponse();

            if (response is ErrorResponse)
            {
                throw new Exception((response as ErrorResponse).message);
            }
            else if (response is GetRoomStateResponse)
            {
                return response as GetRoomStateResponse;
            }
            else
            {
                return null;
            }
        }

        private Question HandleGetQuestionRequest(GetQuestionRequest request)
        {
            SendRequest(request);
            IResponse response = ReceiveResponse();

            if (response is ErrorResponse)
            {
                throw new Exception((response as ErrorResponse).message);
            }
            else if (response is GetQuestionResponse)
            {
                GetQuestionResponse responseQuestion = response as GetQuestionResponse;
                Question question = new Question(responseQuestion.question, responseQuestion.answers);
                return question;
            }
            else
            {
                throw new Exception("No questions remaining");
            }
        }

        private GetGameResultsResponse HandleGetGameResultsRequest(GetGameResultsRequest request)
        {
            SendRequest(request);
            IResponse response = ReceiveResponse();

            if (response is ErrorResponse)
            {
                throw new Exception((response as ErrorResponse).message);
            }
            else if (response is GetGameResultsResponse)
            {
                return response as GetGameResultsResponse;
            }
            else
            {
                return null;
            }
        }

        ~Communicator()
        {
            if (Settings.LoggedUser != "")
                LogOut();

            CloseConnection();
        }
    }
}
