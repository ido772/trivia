// Server.cpp
//

#include "Server.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"
#include "SqliteDataBase.h"

void handleCommands(std::string command);

/*
	Empty Constructor 
*/
Server::Server()
	: m_handlerFactory()
{
	WSADATA wsa_data = { };
	if (WSAStartup(MAKEWORD(2, 2), &wsa_data) != 0)
		throw std::exception("WSAStartup Failed");
	m_database = new SqliteDataBase();
}

/*
	Destructor
*/
Server::~Server()
{
	// Q: why is this try necessary ?
	// A: to avoid throwing exceptions in d-tors !
	// if we do throw think what will happened in regular exception, our object 
	// will be destroyed because an exception occurred and then the d-tor will be 
	// called and if we throw now there is no one to handle the exception because 
	// we are already in the flow of exception handling !! (inception...)
	// please read more about exception handling and why it's forbidden to throw
	// exception from the destructor.
	try
	{
		WSACleanup();
	}
	catch (...) {}
	delete m_database;
}

//
// Methods
//

void Server::Run()
{
	std::cout << "Server initializing" << std::endl;

	// Open a thread to start handling clients requests.
	std::thread communicationThread([this] { this->m_communicator.startHandleRequests(); });
	communicationThread.detach();

	std::string cmd;
	bool isRunning = true;
	do
	{
		// Get a command from user input.
		std::getline(std::cin, cmd);
		std::cout << "Command received: <" << cmd << ">" << std::endl;
		handleCommands(cmd);

		isRunning = (cmd != "EXIT");

	} while (isRunning);

	std::cout << "Server is being terminated" << std::endl;
}

/*
Function to handle server commands.
Input:
	command - The command to handle.
Output: None.
*/
void handleCommands(std::string command)
{
	if (command == "test-error") {
		ErrorResponse err = { "some error" };
		std::cout << JsonResponsePacketSerializer::serializeResponse(err) << std::endl;
	}
	else if (command == "test-login") {
		std::cout << "Request:" << std::endl;
		auto request = JsonRequestPacketDeserializer::deserializeLoginRequest("{ \"username\": \"user1\", \"password\": \"pass123\" }");
		std::cout << "username: " << request.username << ", password: " << request.password << std::endl;
		std::cout << "Response:" << std::endl;
		LoginResponse login = { 1 };
		std::cout << JsonResponsePacketSerializer::serializeResponse(login) << std::endl;
	}
	else if (command == "test-signup") {
		std::cout << "Request:" << std::endl;
		auto request = JsonRequestPacketDeserializer::deserializeSignupRequest("{ \"username\": \"user1\", \"password\": \"pass123\", \"email\": \"tester@mymail.com\" }");
		std::cout << "username: " << request.username << ", password: " << request.password << ", email: " << request.email << std::endl;
		std::cout << "Response:" << std::endl;
		SignupResponse signup = { 2 };
		std::cout << JsonResponsePacketSerializer::serializeResponse(signup) << std::endl;
	}
}
