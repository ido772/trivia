// JsonResponsePacketSerializer.cpp
//

#include "JsonResponsePacketSerializer.h"

#define SIZE_OF_CONTENTSIZE_FIELD 4

/*
Function to serialize an error response.
Input:
	response - An ErrorResponse struct representing the error response.
Output: A buffer with a serialized error response.
*/
std::string JsonResponsePacketSerializer::serializeResponse(ErrorResponse response)
{
	// Serialize a error response content.
	nlohmann::json serializedContent = {
		{"message", response.message}
	};

	return createResponseBuffer(packetResponseCodes::ERROR_RESPONSE, serializedContent.dump());
}

/*
Function to serialize a login response.
Input:
	response - A LoginResponse struct representing the login response.
Output: A buffer with a serialized login response.
*/
std::string JsonResponsePacketSerializer::serializeResponse(LoginResponse response)
{
	// Serialize a login response content.
	nlohmann::json serializedContent = {
		{"status", response.status}
	};

	return createResponseBuffer(packetResponseCodes::LOGIN_RESPONSE, serializedContent.dump());
}

/*
Function to serialize a signup response.
Input:
	response - A SignupResponse struct representing the singup response.
Output: A buffer with a serialized signup response.
*/
std::string JsonResponsePacketSerializer::serializeResponse(SignupResponse response)
{
	// Serialize a signup response content.
	nlohmann::json serializedContent = {
		{"status", response.status}
	};

	return createResponseBuffer(packetResponseCodes::SIGNUP_RESPONSE, serializedContent.dump());
}

/*
Function to serialize a logout response.
Input:
	response - A SignupResponse struct representing the logout response.
Output: A buffer with a serialized logout response.
*/
std::string JsonResponsePacketSerializer::serializeResponse(LogoutResponse response)
{
	// Serialize a logout response content.
	nlohmann::json serializedContent = {
		{"status", response.status}
	};

	return createResponseBuffer(packetResponseCodes::LOGOUT_RESPONSE, serializedContent.dump());
}

/*
Function to serialize a get-rooms response.
Input:
	response - A SignupResponse struct representing the get-rooms response.
Output: A buffer with a serialized get-rooms response.
*/
std::string JsonResponsePacketSerializer::serializeResponse(GetRoomsResponse response)
{
	// Serialize a get-rooms response status.
	nlohmann::json serializedContent = {
		{"status", response.status}
	};

	// Serialize the RoomData vector.
	nlohmann::json roomsList = nlohmann::json::array();
	for (auto roomData : response.rooms)
	{
		nlohmann::json currRoom = {
			{"id", roomData.id},
			{"isActive", roomData.isActive},
			{"maxPlayers", roomData.maxPlayers},
			{"name", roomData.name},
			{"timePerQuestion", roomData.timePerQuestion}
		};

		roomsList.push_back(currRoom);
	}
	serializedContent["rooms"] = roomsList;

	return createResponseBuffer(packetResponseCodes::GET_ROOMS_RESPONSE, serializedContent.dump());
}

/*
Function to serialize a get-players response.
Input:
	response - A SignupResponse struct representing the get-players response.
Output: A buffer with a serialized get-players response.
*/
std::string JsonResponsePacketSerializer::serializeResponse(GetPlayeresInRoomResponse response)
{
	// Serialize a get-players response content.
	nlohmann::json serializedContent = {
		{"status", response.status}
	};

	// Serialize the players vector.
	nlohmann::json playersList = nlohmann::json::array();
	for (auto player : response.players)
	{
		playersList.push_back(player);
	}
	serializedContent["players"] = playersList;

	return createResponseBuffer(packetResponseCodes::GET_PLAYERS_IN_ROOM_RESPONSE, serializedContent.dump());
}

/*
Function to serialize a get-statistics response.
Input:
	response - A SignupResponse struct representing the get-statistics response.
Output: A buffer with a serialized get-statistics response.
*/
std::string JsonResponsePacketSerializer::serializeResponse(GetStatisticsResponse response)
{
	// Serialize a get-statistics response content.
	nlohmann::json serializedContent = {
		{"status", response.status}
	};

	// Serialize the statistics vector.
	nlohmann::json statisticsList = nlohmann::json::array();
	for (auto stat : response.stats)
	{
		statisticsList.push_back(stat);
	}
	serializedContent["statistics"] = statisticsList;

	return createResponseBuffer(packetResponseCodes::GET_STATISTICS_RESPONSE, serializedContent.dump());
}

/*
Function to serialize a join-room response.
Input:
	response - A SignupResponse struct representing the join-room response.
Output: A buffer with a serialized join-room response.
*/
std::string JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse response)
{
	// Serialize a join-room response content.
	nlohmann::json serializedContent = {
		{"status", response.status}
	};

	return createResponseBuffer(packetResponseCodes::JOIN_ROOM_RESPONSE, serializedContent.dump());
}

/*
Function to serialize a create-room response.
Input:
	response - A SignupResponse struct representing the create-room response.
Output: A buffer with a serialized create-room response.
*/
std::string JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse response)
{
	// Serialize a create-room response content.
	nlohmann::json serializedContent = {
		{"status", response.status},
		{"roomId", response.roomId}
	};

	return createResponseBuffer(packetResponseCodes::CREATE_ROOM_RESPONSE, serializedContent.dump());
}

/*
Function to serialize a close-room response.
Input:
	response - A CloseRoomResponse struct representing the close-room response.
Output: A buffer with a serialized close-room response.
*/
std::string JsonResponsePacketSerializer::serializeResponse(CloseRoomResponse response)
{
	// Serialize a close-room response content.
	nlohmann::json serializedContent = {
		{"status", response.status}
	};

	return createResponseBuffer(packetResponseCodes::CLOSE_ROOM_RESPONSE, serializedContent.dump());
}

/*
Function to serialize a start-game response.
Input:
	response - A StartGameResponse struct representing the start-game response.
Output: A buffer with a serialized start-game response.
*/
std::string JsonResponsePacketSerializer::serializeResponse(StartGameResponse response)
{
	// Serialize a start-game response content.
	nlohmann::json serializedContent = {
		{"status", response.status}
	};

	return createResponseBuffer(packetResponseCodes::START_GAME_RESPONSE, serializedContent.dump());
}

/*
Function to serialize a get-room-state response.
Input:
	response - A GetRoomStateResponse struct representing the get-room-state response.
Output: A buffer with a serialized get-room-state response.
*/
std::string JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse response)
{
	// Serialize a get-room-state response content.
	nlohmann::json serializedContent = {
		{"status", response.status},
		{"hasGameBegun", response.hasGameBegun},
		{"questionCount", response.questionCount},
		{"answerTimeout", response.answerTimeout}
	};

	// Serialize the players vector.
	nlohmann::json playersList = nlohmann::json::array();
	for (auto player : response.players)
	{
		playersList.push_back(player);
	}
	serializedContent["players"] = playersList;

	return createResponseBuffer(packetResponseCodes::GET_ROOM_STATE_RESPONSE, serializedContent.dump());
}

/*
Function to serialize a leave-room response.
Input:
	response - A LeaveRoomResponse struct representing the leave-room response.
Output: A buffer with a serialized leave-room response.
*/
std::string JsonResponsePacketSerializer::serializeResponse(LeaveRoomResponse response)
{
	// Serialize a leave-room response content.
	nlohmann::json serializedContent = {
		{"status", response.status}
	};

	return createResponseBuffer(packetResponseCodes::LEAVE_ROOM_RESPONSE, serializedContent.dump());
}


std::string JsonResponsePacketSerializer::serializeResponse(LeaveGameResponse response)
{
	// Serialize a leave-room response content.
	nlohmann::json serializedContent = {
		{"status", response.status}
	};

	return createResponseBuffer(packetResponseCodes::LEAVE_GAME_RESPONSE, serializedContent.dump());
}

std::string JsonResponsePacketSerializer::serializeResponse(SubmitAnswerResponse response)
{
	// Serialize a leave-room response content.
	nlohmann::json serializedContent = {
		{"status", response.status}
	};

	return createResponseBuffer(packetResponseCodes::SUBMIT_ANSWER_RESPONSE, serializedContent.dump());
}


/*
Function to create a full response buffer.
Input:
	code - The response packet code.
	content - The response packet content.
Output: A full response buffer.
*/
std::string JsonResponsePacketSerializer::createResponseBuffer(packetResponseCodes code, std::string content)
{
	std::stringstream buffer;
	buffer << static_cast<unsigned char>(code) << Helper::getPaddedNumber(content.size(), SIZE_OF_CONTENTSIZE_FIELD) << content;

	return buffer.str();
}

std::string JsonResponsePacketSerializer::serializeResponse(GetQuestionResponse response)
{
	// Serialize a get-room-state response content.
	nlohmann::json serializedContent = {
		{"status", response.status},
		{"question", response.question}
	};

	// Serialize the answers map.
	nlohmann::json answers = nlohmann::json::array();
	for (auto answer : response.possibleAnswers)
	{
		answers.push_back(answer.second);
	}
	serializedContent["answers"] = answers;

	return createResponseBuffer(packetResponseCodes::GET_QUESTION_RESPONSE, serializedContent.dump());
}

std::string JsonResponsePacketSerializer::serializeResponse(GetGameResultsResponse response)
{
	// Serialize a get-room-state response content.
	nlohmann::json serializedContent = {
		{"status", response.status}
	};

	// Serialize the results vector.
	nlohmann::json results = nlohmann::json::array();
	for (auto result : response.results)
	{
		nlohmann::json serializedResult = {
			{"username", result.username},
			{"correctAnswerCount", result.correctAnswerCount},
			{"wrongAnswerCount", result.wrongAnswerCount},
			{"averageAnswerTime", result.averageAnswerTime}
		};
		results.push_back(serializedResult);
	}
	serializedContent["results"] = results;

	return createResponseBuffer(packetResponseCodes::GET_GAME_RESULT_RESPONSE, serializedContent.dump());
}