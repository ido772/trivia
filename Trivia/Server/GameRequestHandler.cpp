#include "GameRequestHandler.h"
#include "Responses.h"

GameRequestHandler::GameRequestHandler(RequestHandlerFactory* requestHandlerFactory,  Game game, LoggedUser user)
	: m_game(game), m_user(user)
{
	m_handlerFactory = requestHandlerFactory;
}


bool GameRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
	return (requestInfo.id == packetRequestCodes::LEAVE_GAME_REQUEST)
		|| (requestInfo.id == packetRequestCodes::GET_QUESTION_REQUEST)
		|| (requestInfo.id == packetRequestCodes::SUBMIT_ANSWER_REQUEST)
		|| (requestInfo.id == packetRequestCodes::GET_GAME_RESULT_REQUEST);
}

RequestResult GameRequestHandler::handleRequest(RequestInfo requestInfo)
{
	RequestResult result;
	try
	{
		switch (requestInfo.id)
		{
		case packetRequestCodes::LEAVE_GAME_REQUEST:
		{
			result = leaveGame(requestInfo);
			result.newHandler = new MenuRequestHandler(m_user, m_handlerFactory);
			break;
		}
		case packetRequestCodes::GET_QUESTION_REQUEST:
		{
			result = getQuestion(requestInfo);
			result.newHandler = this;
			break;
		}
		case packetRequestCodes::SUBMIT_ANSWER_REQUEST:
		{
			result = submitAnswer(requestInfo);
			result.newHandler = this;
			break;
		}
		case packetRequestCodes::GET_GAME_RESULT_REQUEST:
		{
			result = getGameResults(requestInfo);
			result.newHandler = this;
			break;
		}
		default:
			throw std::exception("Invalid message code");
		}
	}
	catch (std::exception e)
	{
		/* Create an error response describing what happened. */
		ErrorResponse error = { e.what() };
		result.newHandler = nullptr;
		result.response = JsonResponsePacketSerializer::serializeResponse(error);
	}
	catch (...)
	{
		ErrorResponse error = { "Unknown error" };
		result.newHandler = nullptr;
		result.response = JsonResponsePacketSerializer::serializeResponse(error);
	}
	return result;
}

RequestResult GameRequestHandler::getQuestion(RequestInfo reqInfo)
{
	SubmitAnswerRequest request;

	Question question = m_handlerFactory->getGameManager()->getQuestionForUser(m_user);
	std::map<int, std::string> possibleAnswers;
	int i = 1;
	for (auto answer : question.getPossibleAnswers())
	{
		possibleAnswers[i++] = answer;
	}

	RequestResult result;
	GetQuestionResponse response = { 1, question.getQuestion(), possibleAnswers };
	result.response = JsonResponsePacketSerializer::serializeResponse(response);
	return result;
}

RequestResult GameRequestHandler::getGameResults(RequestInfo reqInfo)
{
	std::list<PlayerResults> list = m_handlerFactory->getGameManager()->getResults(m_user);
	RequestResult result;
	GetGameResultsResponse response = { m_handlerFactory->getGameManager()->getGame(m_user)->isGameOver(), list };
	result.response = JsonResponsePacketSerializer::serializeResponse(response);
	return result;

}

RequestResult GameRequestHandler::submitAnswer(RequestInfo reqInfo)
{
	SubmitAnswerRequest request;
	request = JsonRequestPacketDeserializer::deserializeSubmitAnswerRequest(reqInfo.buffer);

	m_handlerFactory->getGameManager()->submitAnswer(m_user, request.answerId);

	RequestResult result;
	SubmitAnswerResponse response = { 1 };
	result.response = JsonResponsePacketSerializer::serializeResponse(response);
	return result;
}

RequestResult GameRequestHandler::leaveGame(RequestInfo reqInfo)
{
	m_handlerFactory->getRoomManager()->delUser(m_user);
	RequestResult result;
	LeaveGameResponse response = { 1 };
	result.response = JsonResponsePacketSerializer::serializeResponse(response);
	return result;
}