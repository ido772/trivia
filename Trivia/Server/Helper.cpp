// Helper.cpp
//

#include "Helper.h"

//
// Static Methods
//

std::string Helper::getMessageFromSocket(SOCKET sock)
{
	char* s = getDataFromSocket(sock);
	std::string res(s);
	return res;
}

/*
Function to receive an int part from a socket.
Input:
	sock - The socket.
	bytesNum - The number of bytes to receive as an int from the socket.
Output: The received integer.
*/
int Helper::getIntPartFromSocket(SOCKET sock, int bytesNum)
{
	char* s = getPartFromSocket(sock, bytesNum, 0);
	return atoi(s);
}

/*
Function to receive an string part from a socket.
Input:
	sock - The socket.
	bytesNum - The number of bytes to receive as a string from the socket.
Output: The received string.
*/
std::string Helper::getStringPartFromSocket(SOCKET sock, int bytesNum)
{
	char* s = getPartFromSocket(sock, bytesNum, 0);
	std::string res(s);
	return res;
}

/*
Function to send a string data message through a socket.
Input:
	sock - The socket.
	message -The string data message.
Output: None.
*/
void Helper::sendData(SOCKET sock, std::string message)
{
	const char* data = message.c_str();

	if (send(sock, message.c_str(), message.size(), 0) == INVALID_SOCKET)
	{
		throw std::exception("Error while sending message to client");
	}
}

std::string Helper::getPaddedNumber(int num, int digits)
{
	std::stringstream str;
	str << std::setw(digits) << std::setfill('0') << num;
	return str.str();

}

bool Helper::doesContain(const std::string& str, const std::string& substr)
{
	if (str.find(substr, 0) == std::string::npos)
		return false;
	return true;
}

//
// Private Static Methods
//


char* Helper::getDataFromSocket(SOCKET sock)
{
	char* message = getPartFromSocket(sock, DEFAULT_BUFFER_LEN, 0);
	int x = strlen(message);
	return message;
}

/*
Function to receive an c-string part from a socket.
Input:
	sock - The socket.
	bytesNum - The number of bytes to receive as a c-string from the socket.
Output: The received c-string.
*/
char* Helper::getPartFromSocket(SOCKET sock, int bytesNum)
{
	return getPartFromSocket(sock, bytesNum, 0);
}

/*
Function to receive an c-string part from a socket.
Input:
	sock - The socket.
	bytesNum - The number of bytes to receive as a c-string from the socket.
	flags - Flags for the recv function used in this function.
Output: The received c-string.
*/
char* Helper::getPartFromSocket(SOCKET sock, int bytesNum, int flags)
{
	/* If bytesNum is 0, return an empty string. */
	if (bytesNum == 0)
	{
		return (char*)"";
	}

	int strSize = bytesNum + 1;
	char* data = new char[strSize]();
	int res = recv(sock, data, bytesNum, flags);

	if (res == INVALID_SOCKET)
	{
		std::string s = "Error while recieving from socket s";
		s += std::to_string(sock);
		throw std::exception(s.c_str());
	}

	data[bytesNum] = 0;
	return data;
}
