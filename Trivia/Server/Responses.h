// Responses.h
//

#pragma once
#include "Room.h"
#include <list>

enum packetResponseCodes
{
	ERROR_RESPONSE = 100,
	LOGIN_RESPONSE = 2,
	SIGNUP_RESPONSE = 4,
	LOGOUT_RESPONSE = 6,
	GET_ROOMS_RESPONSE = 8,
	GET_PLAYERS_IN_ROOM_RESPONSE = 10,
	GET_STATISTICS_RESPONSE = 12,
	JOIN_ROOM_RESPONSE = 14,
	CREATE_ROOM_RESPONSE = 16,
	CLOSE_ROOM_RESPONSE = 18,
	START_GAME_RESPONSE = 20,
	GET_ROOM_STATE_RESPONSE = 22,
	LEAVE_ROOM_RESPONSE = 24,
	LEAVE_GAME_RESPONSE = 26,
	SUBMIT_ANSWER_RESPONSE = 28,
	GET_QUESTION_RESPONSE = 30,
	GET_GAME_RESULT_RESPONSE = 32
};

struct ErrorResponse
{
	std::string message;
};

struct LoginResponse
{
	unsigned int status;
};

struct SignupResponse
{
	unsigned int status;
};

struct LogoutResponse
{
	unsigned int status;
};

struct GetRoomsResponse
{
	unsigned int status;
	std::vector<RoomData> rooms;
};

struct GetPlayeresInRoomResponse
{
	unsigned int status;
	std::vector<std::string> players;
};

struct GetStatisticsResponse
{
	unsigned int status;
	std::vector<std::string> stats;
};

struct JoinRoomResponse
{
	unsigned int status;
};

struct CreateRoomResponse
{
	unsigned int status;
	unsigned int roomId;
};

struct CloseRoomResponse
{
	unsigned int status;
};

struct StartGameResponse
{
	unsigned int status;
};

struct GetRoomStateResponse
{
	unsigned int status;
	bool hasGameBegun;
	std::vector<std::string> players;
	unsigned int questionCount;
	unsigned int answerTimeout;
};

struct LeaveRoomResponse
{
	unsigned int status;
};

struct LeaveGameResponse
{
	unsigned int status;
};

struct SubmitAnswerResponse
{
	unsigned int status;
}; 

struct GetQuestionResponse
{
	unsigned int status;
	std::string question;
	std::map<int, std::string> possibleAnswers;
};

struct PlayerResults
{
	std::string username;
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	float averageAnswerTime;
};

struct GetGameResultsResponse
{
	unsigned int status;
	std::list< PlayerResults> results;
};