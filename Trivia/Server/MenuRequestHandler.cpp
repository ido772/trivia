// MenuRequestHandler.cpp
//

#include "MenuRequestHandler.h"
#include "RequestHandlerFactory.h"

MenuRequestHandler::MenuRequestHandler()
	: m_user("")
{
	this->m_handlerFactory = new RequestHandlerFactory();
}

MenuRequestHandler::MenuRequestHandler(LoggedUser user, RequestHandlerFactory* request)
	: m_user(user)
{
	this->m_handlerFactory = request;
}

MenuRequestHandler::~MenuRequestHandler()
{
	delete this->m_handlerFactory;
}

/*
Function to check if a request is relevant.
Input:
	requestInfo - Information about the request to check.
Output: whether or not the request was found relevant.
*/
bool MenuRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
	return (requestInfo.id == packetRequestCodes::LOGOUT_REQUEST)
		|| (requestInfo.id == packetRequestCodes::GET_ROOMS_REQUEST)
		|| (requestInfo.id == packetRequestCodes::GET_PLAYERS_IN_ROOM_REQUEST)
		|| (requestInfo.id == packetRequestCodes::GET_STATISTICS_REQUEST)
		|| (requestInfo.id == packetRequestCodes::JOIN_ROOM_REQUEST)
		|| (requestInfo.id == packetRequestCodes::CREATE_ROOM_REQUEST);
}

/*
Function to handle a menu related request.
Input:
	requestInfo - Information about the menu request.
Output: The result (response) of handling the request.
*/
RequestResult MenuRequestHandler::handleRequest(RequestInfo requestInfo)
{
	RequestResult result;

	try
	{
		switch (requestInfo.id)
		{
		case packetRequestCodes::LOGOUT_REQUEST:
		{
			result = signout(requestInfo);
			result.newHandler = nullptr;
			break;
		}
		case packetRequestCodes::GET_ROOMS_REQUEST:
		{
			result = getRooms(requestInfo);
			result.newHandler = this;
			break;
		}
		case packetRequestCodes::GET_PLAYERS_IN_ROOM_REQUEST:
		{
			result = getPlayers(requestInfo);
			result.newHandler = this;
			break;
		}
		case packetRequestCodes::GET_STATISTICS_REQUEST:
		{
			result = getStatistics(requestInfo);
			result.newHandler = this;
			break;
		}
		case packetRequestCodes::JOIN_ROOM_REQUEST:
		{
			result = joinRoom(requestInfo);
			break;
		}
		case packetRequestCodes::CREATE_ROOM_REQUEST:
		{
			
			result = createRoom(requestInfo);
			result.newHandler = m_handlerFactory->createRoomAdminRequestHandler(m_user);
			break;
		}
		default:
			throw std::exception("Invalid message code");
		}
	}
	catch (std::exception e)
	{
		/* Create an error response describing what happened. */
		ErrorResponse error = { e.what() };
		result.newHandler = nullptr;
		result.response = JsonResponsePacketSerializer::serializeResponse(error);
	}
	catch (...)
	{
		ErrorResponse error = { "Unknown error" };
		result.newHandler = nullptr;
		result.response = JsonResponsePacketSerializer::serializeResponse(error);
	}

	return result;
}

void MenuRequestHandler::setUser(LoggedUser user)
{
	this->m_user = user;
}

/*
Function to handle a signout request.
Input:
	info - The request info.
Output: A request result.
*/
RequestResult MenuRequestHandler::signout(RequestInfo requestInfo)
{
	if (m_user.getUsername() == "") {
		throw std::exception("A player must be logged in to log out");
	}

	RequestResult result;

	LogoutResponse response;
	response.status = 1;

	// Log the user out.
	m_handlerFactory->getLoginManager()->logout(m_user.getUsername());

	result.response = JsonResponsePacketSerializer::serializeResponse(response);
	return result;
}

/*
Function to handle a get-rooms request.
Input:
	info - The request info.
Output: A request result.
*/
RequestResult MenuRequestHandler::getRooms(RequestInfo requestInfo)
{
	RequestResult result;

	GetRoomsResponse response;

	// Get all of the rooms.
	for (auto room : m_handlerFactory->getRoomManager()->getRooms())
	{
		response.rooms.push_back(room.getRoomData());
	}

	response.status = 1;
	result.response = JsonResponsePacketSerializer::serializeResponse(response);

	return result;
}

/*
Function to handle a get-players request.
Input:
	info - The request info.
Output: A request result.
*/
RequestResult MenuRequestHandler::getPlayers(RequestInfo requestInfo)
{
	RequestResult result;

	GetPlayersInRoomRequest request;
	request = JsonRequestPacketDeserializer::deserializeGetPlayersRequest(requestInfo.buffer);

	GetPlayeresInRoomResponse response;

	// Get all players in the room.
	for (auto room : m_handlerFactory->getRoomManager()->getRooms())
	{
		// Find the wanted room.
		if (room.getRoomData().id == request.roomId)
		{
			// Get all players in this room.
			for (auto player : room.getAllUsers())
			{
				response.players.push_back(player.getUsername());
			}

			break;		// Quit the loop.
		}
	}

	result.response = JsonResponsePacketSerializer::serializeResponse(response);
	return result;
}

/*
Function to handle a get-statistics request.
Input:
	info - The request info.
Output: A request result.
*/
RequestResult MenuRequestHandler::getStatistics(RequestInfo requestInfo)
{
	RequestResult result;

	GetStatisticsResponse response;

	// Get the statistics of the top 5 users.
	for (auto stat : m_handlerFactory->getStatsManager()->getTop5())
	{
		// Create a string representing the user stats.
		std::string statString = stat.userName + ":";
		statString += "\"average_answer_time\"=" + std::to_string(stat.averageAnswerTime) + ",";
		statString += "\"correct_answers\"=" + std::to_string(stat.correctAnswers) + ",";
		statString += "\"num_of_games\"=" + std::to_string(stat.numOfGames) + ",";
		statString += "\"total_answers\"=" + std::to_string(stat.totalAnswers);

		// Add the string to the response.
		response.stats.push_back(statString);
	}

	result.response = JsonResponsePacketSerializer::serializeResponse(response);
	return result;
}

/*
Function to handle a join-room request.
Input:
	info - The request info.
Output: A request result.
*/
RequestResult MenuRequestHandler::joinRoom(RequestInfo requestInfo)
{
	if (m_user.getUsername() == "") {
		throw std::exception("A player must be logged in to join a room");
	}

	RequestResult result;

	JoinRoomRequest request;
	request = JsonRequestPacketDeserializer::deserializeJoinRoomRequest(requestInfo.buffer);

	// Add the user to the room.
	m_handlerFactory->getRoomManager()->addUser(request.roomId, m_user);

	// Add the user to the game.
	//m_handlerFactory->getGameManager()->

	for (auto room : m_handlerFactory->getRoomManager()->getRooms()) {
		if (room.getRoomData().id == request.roomId) {
			result.newHandler = m_handlerFactory->createRoomMemberRequestHandler(room, m_user);
			break;
		}
	}

	JoinRoomResponse response = { 1 };
	result.response = JsonResponsePacketSerializer::serializeResponse(response);

	return result;
}

/*
Function to handle a create-room request.
Input:
	info - The request info.
Output: A request result.
*/
RequestResult MenuRequestHandler::createRoom(RequestInfo requestInfo)
{
	if (m_user.getUsername() == "") {
		throw std::exception("A player must be logged in to create a room");
	}

	RequestResult result;

	CreateRoomRequest request;
	request = JsonRequestPacketDeserializer::deserializeCreateRoomRequest(requestInfo.buffer);

	// Create a room.
	RoomData metaData = {0, request.name, request.maxPlayers, request.answerTimeout, 0};
	m_handlerFactory->getRoomManager()->createRoom(m_user, metaData);

	//// Create a game for the room.
	//for (auto room : m_handlerFactory->getRoomManager()->getRooms()) {
	//	if (room.getRoomData().name == metaData.name) {
	//		m_handlerFactory->getGameManager()->createRoom(room);
	//	}
	//}

	CreateRoomResponse response = { 1, metaData.id };
	result.response = JsonResponsePacketSerializer::serializeResponse(response);

	return result;
}
