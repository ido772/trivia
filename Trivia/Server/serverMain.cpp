// serverMain.cpp
//
#include "Server.h"
#define VERSION "1.0.1"

int main()
{
	std::cout << "=============================" << std::endl
		<< "Trivia server V" << VERSION << std::endl
		<< "=============================" << std::endl;

	/* Start the server and run it. */
	Server server;
	server.Run();

	return 0;
}
