#include "LoginManager.h"
#include "SqliteDataBase.h"
#include "User.h"


/*
	Constructor
*/
LoginManager::LoginManager()
{
	this->m_database = new SqliteDataBase();
}

/*
	Destructor
*/
LoginManager::~LoginManager()
{
	delete m_database;
}


/*
Thid method signs up a new user 
Input: userName - name of new user
       password - password of new user
	   email - email of new user
Output: none
*/
void LoginManager::signup(const std::string& userName, const std::string& password, const std::string& email)
{
	if (this->m_database->doesUserExists(userName)) {
		throw std::exception("Username taken");
	}

	User user(userName, password, email);
	this->m_database->createUser(user);
}


/*
This method login a user(if exists)
Input: userName - name of user to log in
	   password - password of user to log in
Output: none
*/
void LoginManager::login(const std::string& userName, const std::string& password)
{
	if (!this->m_database->doesUserExists(userName))
	{
		throw std::exception("Login failed");
	}

	if (this->m_database->doesPasswordMatch(userName, password))
	{
		this->m_loggedUsers.push_back(LoggedUser(userName));
	}
}


/*
This method logouts a specifc user if the user logged in
Input: userName - user to logout
Output: none
*/
void LoginManager::logout(const std::string& userName)
{
	for (std::vector<LoggedUser>::iterator i = this->m_loggedUsers.begin(); i != this->m_loggedUsers.end(); i++)
	{
		if (userName == i->getUsername())
		{
			this->m_loggedUsers.erase(i);
			break;
		}
	}
}