#include "RoomAdminRequestHandler.h"

RoomAdminRequestHandler::RoomAdminRequestHandler(RequestHandlerFactory* handlerFactory, LoggedUser user, Room room)
	: m_user(user), m_handlerFactory(handlerFactory), m_room(room)
{
}

RequestResult RoomAdminRequestHandler::handleRequest(RequestInfo requestInfo)
{
	RequestResult result;
	try
	{
		switch (requestInfo.id)
		{
		case packetRequestCodes::LEAVE_ROOM_REQUEST:
		{
			result = closeRoom(requestInfo);
			result.newHandler = new MenuRequestHandler(m_user, m_handlerFactory);
			break;
		}
		case packetRequestCodes::CLOSE_ROOM_REQUEST:
		{
			result = closeRoom(requestInfo);
			result.newHandler = new MenuRequestHandler(m_user, m_handlerFactory);
			break;
		}
		case packetRequestCodes::START_GAME_REQUEST:
		{
			result = startGame(requestInfo);
			result.newHandler = m_handlerFactory->createGameRequestHandler(m_user);
			break;
		}
		case packetRequestCodes::GET_ROOM_STATE_REQUEST:
		{
			result = getRoomState(requestInfo);
			result.newHandler = this;
			break;
		}
		default:
			throw std::exception("Invalid message code");
		}
	}
	catch (std::exception e)
	{
		/* Create an error response describing what happened. */
		ErrorResponse error = { e.what() };
		result.newHandler = nullptr;
		result.response = JsonResponsePacketSerializer::serializeResponse(error);
	}
	catch (...)
	{
		ErrorResponse error = { "Unknown error" };
		result.newHandler = nullptr;
		result.response = JsonResponsePacketSerializer::serializeResponse(error);
	}
	return result;
}


RequestResult RoomAdminRequestHandler::getRoomState(RequestInfo requestInfo)
{
	int roomState = m_handlerFactory->getRoomManager()->getRoomState(m_room.getRoomData().id);

	RequestResult result;
	GetRoomStateResponse response = {roomState, STARTED == roomState, m_handlerFactory->getRoomManager()->getUsers(m_room.getRoomData().id) , 0, 0};
	result.response = JsonResponsePacketSerializer::serializeResponse(response);

	return result;
}


bool RoomAdminRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
	return (requestInfo.id == packetRequestCodes::CLOSE_ROOM_REQUEST)
		|| (requestInfo.id == packetRequestCodes::START_GAME_REQUEST)
		|| (requestInfo.id == packetRequestCodes::GET_ROOM_STATE_REQUEST);
}

RequestResult RoomAdminRequestHandler::closeRoom(RequestInfo requestInfo)
{
	m_handlerFactory->getRoomManager()->deleteRoom(m_room.getRoomData().id);

	RequestResult result;
	CloseRoomResponse response = { 1 };
	result.response = JsonResponsePacketSerializer::serializeResponse(response);
	return result;
}

RequestResult RoomAdminRequestHandler::startGame(RequestInfo requestInfo)
{
	m_handlerFactory->getRoomManager()->startGame(m_room.getRoomData().id);

	// Create the game.
	for (auto room : m_handlerFactory->getRoomManager()->getRooms()) {
		if (room.getRoomData().id == m_room.getRoomData().id) {
			m_handlerFactory->getGameManager()->createRoom(room);
		}
	}

	RequestResult result;
	StartGameResponse response = { 1 };
	result.response = JsonResponsePacketSerializer::serializeResponse(response);
	return result;
}