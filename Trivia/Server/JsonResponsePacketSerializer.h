// JsonResponsePacketSerializer.h
//

#pragma once
#include "nlohmann/json.hpp"
#include "Helper.h"
#include "Responses.h"

class JsonResponsePacketSerializer
{
public:
	static std::string serializeResponse(ErrorResponse response);
	static std::string serializeResponse(LoginResponse response);
	static std::string serializeResponse(SignupResponse response);
	static std::string serializeResponse(LogoutResponse response);
	static std::string serializeResponse(GetRoomsResponse response);
	static std::string serializeResponse(GetPlayeresInRoomResponse response);
	static std::string serializeResponse(GetStatisticsResponse response);
	static std::string serializeResponse(JoinRoomResponse response);
	static std::string serializeResponse(CreateRoomResponse response);
	static std::string serializeResponse(CloseRoomResponse response);
	static std::string serializeResponse(StartGameResponse response);
	static std::string serializeResponse(GetRoomStateResponse response);
	static std::string serializeResponse(LeaveRoomResponse response);
	static std::string serializeResponse(LeaveGameResponse response); 
	static std::string serializeResponse(SubmitAnswerResponse response);
	static std::string serializeResponse(GetQuestionResponse response);
	static std::string serializeResponse(GetGameResultsResponse response);

private:
	static std::string createResponseBuffer(packetResponseCodes code, std::string content);
};

