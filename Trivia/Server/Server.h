// Server.cpp
//

#pragma once
#include "Communicator.h"
#include "RequestHandlerFactory.h"
#include "IDatabase.h"

class Server
{
public:
	Server();
	~Server();

	void Run();

private:
	Communicator m_communicator;
	RequestHandlerFactory m_handlerFactory;
	IDatabase* m_database;
};

