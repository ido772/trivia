// Requests.h
//

#pragma once
#include <string>

enum packetRequestCodes
{
	SIGNUP_REQUEST = 1,
	LOGIN_REQUEST = 3,
	LOGOUT_REQUEST = 5,
	GET_ROOMS_REQUEST = 7,
	GET_PLAYERS_IN_ROOM_REQUEST = 9,
	GET_STATISTICS_REQUEST = 11,
	JOIN_ROOM_REQUEST = 13,
	CREATE_ROOM_REQUEST = 15,
	CLOSE_ROOM_REQUEST = 17,
	START_GAME_REQUEST = 19,
	GET_ROOM_STATE_REQUEST = 21,
	LEAVE_ROOM_REQUEST = 23,
	LEAVE_GAME_REQUEST = 25,
	GET_QUESTION_REQUEST = 27,
	SUBMIT_ANSWER_REQUEST = 29,
	GET_GAME_RESULT_REQUEST = 31
};

struct LoginRequest
{
	std::string username;
	std::string password;
};

struct SignupRequest
{
	std::string username;
	std::string password;
	std::string email;
};

struct GetPlayersInRoomRequest
{
	unsigned int roomId;
};

struct JoinRoomRequest
{
	unsigned int roomId;
};

struct CreateRoomRequest
{
	std::string name;
	unsigned int maxPlayers;
	unsigned int questionsCount;
	unsigned int answerTimeout;
};

struct SubmitAnswerRequest
{
	unsigned int answerId;
};