#pragma once
#include "LoginManager.h"
#include "RoomManager.h"
#include "StatisticsManager.h"
#include "IDatabase.h"
#include "LoginRequestHandler.h"
#include "MenuRequestHandler.h"
#include "RoomAdminRequestHandler.h"
#include "RoomMemberRequestHandler.h"
#include "GameManager.h"
#include "GameRequestHandler.h"

class RoomMemberRequestHandler;
class RoomAdminRequestHandler;
class GameRequestHandler;

class RequestHandlerFactory
{
public:
	RequestHandlerFactory();
	~RequestHandlerFactory();

	LoginRequestHandler createLoginRequestHandler();
	MenuRequestHandler createMenuRequestHandler();
	RoomMemberRequestHandler* createRoomMemberRequestHandler(Room room, LoggedUser user);
	RoomAdminRequestHandler* createRoomAdminRequestHandler(LoggedUser user);
	GameRequestHandler* createGameRequestHandler(LoggedUser user);

	LoginManager* getLoginManager();		// getter
	RoomManager* getRoomManager();			// getter
	StatisticsManager* getStatsManager();	// getter
	GameManager* getGameManager();			// getter

private:
	LoginManager* m_loginManager;
	IDatabase* m_dataBase;
	RoomManager* m_roomManager;
	StatisticsManager* m_statsManager;
	GameManager* m_gameManager;
};