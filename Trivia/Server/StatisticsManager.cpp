#include "StatisticsManager.h"
#include "SqliteDataBase.h"

StatisticsManager::StatisticsManager()
{
	m_database = new SqliteDataBase();
}

StatisticsManager::~StatisticsManager()
{
	delete m_database;
}

userStatistics StatisticsManager::getUserStatistics(std::string userName)
{
	userStatistics userInfo = { userName,
								m_database->getPlayerAverageAnswerTime(userName),
								m_database->getNumOfCorrectAnswers(userName),
								m_database->getNumOfTotalAnswers(userName),
								m_database->getNumOfPlayerGames(userName)};
	return userInfo;
}

std::vector<userStatistics> StatisticsManager::getTop5()
{
	return m_database->getTop5Players();
}