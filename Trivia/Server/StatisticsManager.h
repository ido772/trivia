#pragma once
#include "IDatabase.h"
#include <vector>

class StatisticsManager
{
public:
	StatisticsManager();
	~StatisticsManager();

	userStatistics getUserStatistics(std::string userName);
	std::vector<userStatistics> getTop5();

private:
	IDatabase* m_database;
};