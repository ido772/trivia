#pragma once
#include "Question.h"
#include "LoggedUser.h"
#include <map>
#include <vector>
#include <chrono>
#include <ctime>

typedef struct GameData
{
	Question currentQuestion;
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	float averageAnswerTime;
	std::chrono::system_clock::time_point lastAnswerTime;
} GameData;

class Game
{
public:
	Game(std::vector<Question> questions, std::vector<LoggedUser> users);
	Game() = default;
	~Game() = default;

	Question getQuestionForUser(LoggedUser user);
	void submitAnswer(LoggedUser, int ans);
	void removePlayer(LoggedUser user);
	unsigned int isGameOver();
	std::map<LoggedUser, GameData> getPlayers() { return m_players; };

private:
	std::vector<Question>  m_questions;
	std::map<LoggedUser, GameData> m_players;
};