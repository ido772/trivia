// IRequestHandler.h
//

#pragma once
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"

class IRequestHandler;

struct RequestInfo
{
	int id;
	time_t receivalTime;
	std::string buffer;
};

struct RequestResult
{
	std::string response;
	IRequestHandler* newHandler;
};

class IRequestHandler
{
public:
	virtual bool isRequestRelevant(RequestInfo requestInfo) = 0;
	virtual RequestResult handleRequest(RequestInfo requestInfo) = 0;

};

