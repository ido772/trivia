#pragma once
#include "IDatabase.h"
#include "sqlite3.h"
#include <sstream>

#define DATA_BASE_NAME "triviaDB.sqlite"
#define DATA_BASE_NOT_EXISTS 0


class SqliteDataBase : public IDatabase
{
public:
	SqliteDataBase();
	~SqliteDataBase(); 

	// user related
	virtual bool doesUserExists(const std::string& userName) override;
	virtual bool doesPasswordMatch(const std::string& userName, const std::string& password) override;
	virtual void createUser(User& user) override;

	// Question related
	virtual std::vector<QuestionDB> getQuestions(int questionsAmount);

	// Statistics related 
	virtual float getPlayerAverageAnswerTime(std::string userName);
	virtual int getNumOfCorrectAnswers(std::string userName);
	virtual int getNumOfTotalAnswers(std::string userName);
	virtual int getNumOfPlayerGames(std::string userName);
	virtual std::vector<userStatistics> getTop5Players();

private:
	sqlite3* _db;

	/* Helper methods */
	void initDB();
	bool open() override;
	void close() override;

	/* CallBack methods */
	static int callbackUsers(void* data, int argc, char** argv, char** azColName);
	static int callbackQuestions(void* data, int argc, char** argv, char** azColName);
	static int callbackUserStatistics(void* data, int argc, char** argv, char** azColName);

	void executeStatement(const char* statement, char* errMsg, int(*callBack)(void*, int, char**, char**), void* data);

	void addDefaultQuestions();
};





