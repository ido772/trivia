// MenuRequestHanlder.h
//

#pragma once
#include "IRequestHandler.h"


class RequestHandlerFactory;

class MenuRequestHandler : public IRequestHandler
{
public:
	MenuRequestHandler();
	MenuRequestHandler(LoggedUser user, RequestHandlerFactory* request);
	~MenuRequestHandler();

	virtual bool isRequestRelevant(RequestInfo requestInfo) override;
	virtual RequestResult handleRequest(RequestInfo requestInfo) override;

	void setUser(LoggedUser user);

private:
	RequestHandlerFactory* m_handlerFactory;
	LoggedUser m_user;

	RequestResult signout(RequestInfo requestInfo);
	RequestResult getRooms(RequestInfo requestInfo);
	RequestResult getPlayers(RequestInfo requestInfo);
	RequestResult getStatistics(RequestInfo requestInfo);
	RequestResult joinRoom(RequestInfo requestInfo);
	RequestResult createRoom(RequestInfo requestInfo);

};

