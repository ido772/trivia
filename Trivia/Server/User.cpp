﻿#include "User.h"
#include <iomanip>


User::User(const std::string& name, const std::string& password, const std::string& email) :
	m_name(name), m_password(password), m_email(email)
{
	// Left empty
}

const std::string& User::getName() const
{
	return m_name;
}

void User::setName(const std::string& name)
{
	m_name = name;
}

const std::string& User::getPassword() const
{
	return m_password;
}

void User::setPassword(const std::string& password)
{
	m_password = password;
}

const std::string& User::getEmail() const
{
	return m_email;
}

void User::setEmail(const std::string& email)
{
	m_email = email;
}

bool User::operator==(const User& other) const
{
	return m_name == other.m_name;
}

bool User::operator==(const std::string& name) const
{
	return m_name == name;
}


std::ostream& operator<<(std::ostream& strOut, const User& user)
{
	strOut << std::setw(5) <<"   + @" << user.m_name << "-" << user.m_password << " - " << user.m_email;
	return strOut;
}