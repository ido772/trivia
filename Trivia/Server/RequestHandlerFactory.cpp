#include "RequestHandlerFactory.h"
#include "SqliteDataBase.h"
#include "GameRequestHandler.h"

/*
	Default Constructor
*/
RequestHandlerFactory::RequestHandlerFactory()
{
	m_loginManager = new LoginManager();
	m_dataBase = new SqliteDataBase();
	m_roomManager = new RoomManager();
	m_statsManager = new StatisticsManager();
	m_gameManager = new GameManager(m_dataBase);
}


/*
	Default Destructor
*/
RequestHandlerFactory::~RequestHandlerFactory()
{
	delete m_loginManager;
	delete m_dataBase;
	delete m_roomManager;
	delete m_statsManager;
	delete m_gameManager;
}

/*
This method creates a handle for dealing with login request
Output: none
Input: returns an 
*/
LoginRequestHandler RequestHandlerFactory::createLoginRequestHandler()
{
	return LoginRequestHandler(this);
}

MenuRequestHandler RequestHandlerFactory::createMenuRequestHandler()
{
	return MenuRequestHandler();
}

/*
This method gets the login manager
Input: none
Output: returns login manager
*/
LoginManager* RequestHandlerFactory::getLoginManager()
{
	return this->m_loginManager;
}

RoomManager* RequestHandlerFactory::getRoomManager()
{
	return this->m_roomManager;
}

StatisticsManager* RequestHandlerFactory::getStatsManager()
{
	return this->m_statsManager;
}

GameManager* RequestHandlerFactory::getGameManager()
{
	return this->m_gameManager;
}

RoomMemberRequestHandler* RequestHandlerFactory::createRoomMemberRequestHandler(Room room, LoggedUser user)
{
	return new RoomMemberRequestHandler(this, user, room);
}

RoomAdminRequestHandler* RequestHandlerFactory::createRoomAdminRequestHandler(LoggedUser user)
{
	return new RoomAdminRequestHandler(this, user, m_roomManager->getRooms().back());
}

GameRequestHandler* RequestHandlerFactory::createGameRequestHandler(LoggedUser user)
{
	Game* game = (m_gameManager->getGame(user));
	return new GameRequestHandler(this, *game, user);
}