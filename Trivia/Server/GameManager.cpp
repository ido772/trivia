#include "GameManager.h"
#include "IDatabase.h"

GameManager::GameManager(IDatabase* database) : m_database(database), m_games()
{
}


void GameManager::createRoom(Room room)
{
	m_games.emplace_back(Game(Question::convert_vector_struct_to_question(m_database->getQuestions(11)), room.getAllUsers()));
}

Question GameManager::getQuestionForUser(LoggedUser user)
{
	Question question;
	for (auto& game : m_games)
	{
		for (auto player : game.getPlayers())
		{
			if (((LoggedUser)(player.first)).getUsername() == user.getUsername())
			{
				question = game.getQuestionForUser(user);
			}
		}
	}
	return question;
}

void GameManager::submitAnswer(LoggedUser user, int ans)
{
	for (auto& game : m_games)
	{
		for (auto player : game.getPlayers())
		{
			if (((LoggedUser)(player.first)).getUsername() == user.getUsername())
			{
				game.submitAnswer(user, ans);
				return;
			}
		}
	}
}

std::list<PlayerResults> GameManager::getResults(LoggedUser user)
{
	bool flag = false;
	std::list<PlayerResults> list;
	PlayerResults result;
	for (auto game : m_games)
	{
		for (auto player : game.getPlayers())
		{
			if (((LoggedUser)(player.first)).getUsername() == user.getUsername())
			{
				flag = true;
			}
		}
		if (flag)
		{
			for (auto player : game.getPlayers())
			{
				result = { ((LoggedUser)(player.first)).getUsername(), player.second.correctAnswerCount, player.second.wrongAnswerCount, player.second.averageAnswerTime };
				list.emplace_back(result);
			}
		}
	}
	return list;
}

Game* GameManager::getGame(LoggedUser user)
{
	for (auto& game : m_games)
	{
		for (auto player : game.getPlayers())
		{
			if (((LoggedUser)(player.first)).getUsername() == user.getUsername())
			{
				return &game;
			}
		}
	}
	return new Game();
}
