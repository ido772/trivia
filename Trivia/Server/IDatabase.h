#pragma once
#include <string>
#include <vector>
#include "User.h"

#define WRONG_ANSWERS_AMOUNT 3
struct QuestionDB
{
	std::string question;
	std::string correctAnswer;
	std::string wrong_answers[WRONG_ANSWERS_AMOUNT];
}typedef QuestionDB;

struct userStatistics
{
	std::string userName;
	float averageAnswerTime;
	int correctAnswers;
	int totalAnswers;
	int numOfGames;
}typedef Statistics;


class IDatabase
{
public:
	virtual ~IDatabase() = default;

	// user related
	virtual bool doesUserExists(const std::string& userName) = 0;
	virtual bool doesPasswordMatch(const std::string& userName, const std::string& password) = 0;
	virtual void createUser(User& user) = 0;

	// Question related
	virtual std::vector<QuestionDB> getQuestions(int questionsAmount) = 0;

	// Statistics related 
	virtual float getPlayerAverageAnswerTime(std::string userName) = 0;
	virtual int getNumOfCorrectAnswers(std::string userName) = 0;
	virtual int getNumOfTotalAnswers(std::string userName) = 0;
	virtual int getNumOfPlayerGames(std::string userName) = 0;
	virtual std::vector<userStatistics> getTop5Players() = 0;

private:
	virtual bool open() = 0;
	virtual void close() = 0;
};



