#include "LoggedUser.h"

/*
	Constructor 
input: username - username to be setted of the logged user
Output: none
*/
LoggedUser::LoggedUser(const std::string username) : m_username(username)
{
	/* Empty */
}

/*
This method returns the name of the username
Input: none
Output: name of the user
*/
bool LoggedUser::operator<(const LoggedUser& rhs) const
{
	return m_username < rhs.m_username;
}

std::string LoggedUser::getUsername()
{
	return m_username;
}
