#pragma once
#include <string>
#include <vector>
#include "LoggedUser.h"

enum roomState{NOT_ACTIVE, ACTIVE, STARTED};

typedef struct RoomData
{
	unsigned int id;
	std::string name;
	unsigned int maxPlayers;
	unsigned int timePerQuestion;
	unsigned int isActive;
} RoomData;


class Room
{
public:
	Room(RoomData metaData);

	Room() = default;
	~Room() = default;

	void addUser(LoggedUser& user);
	void removeUser(LoggedUser& user);
	std::vector<LoggedUser> getAllUsers();
	void startGame();
	unsigned int getRoomState();
	RoomData getRoomData();
	
private:
	RoomData m_metaData;
	std::vector<LoggedUser> m_users;
};
