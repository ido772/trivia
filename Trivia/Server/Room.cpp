#include "Room.h"


/*
	Constructor
Input: metaData - data related to the room
*/
Room::Room(RoomData metaData)  
{
	m_metaData = metaData;
	m_users.clear();
}

/*
This method adds user
Input: user - user to be added
Output: none
*/
void Room::addUser(LoggedUser & user)
{
	m_users.push_back(user);
}


/*
This method removes the user 
Input: user - user to be removed
Output: none
*/
void Room::removeUser(LoggedUser& user)
{
	for (std::vector<LoggedUser>::iterator i = m_users.begin(); i != this->m_users.end(); i++)
	{
		if (user.getUsername() == i->getUsername())
		{
			this->m_users.erase(i);
			break;
		}
	}
}


/*
This method gets all of the logged users to the room
Input: none
Output: returns the logged the user
*/
std::vector<LoggedUser> Room::getAllUsers()
{
	return m_users;
}


/*
This method gets the room state
Input: none
Output: the romm state
*/
unsigned int Room::getRoomState()
{
	return m_metaData.isActive;
}

RoomData Room::getRoomData()
{
	return this->m_metaData;
}

void Room::startGame()
{
	m_metaData.isActive = STARTED;
}