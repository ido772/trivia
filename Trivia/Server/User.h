﻿#pragma once
#include <string>
#include <iostream>

class User
{
public:
	
	User(const std::string& name, const std::string& password, const std::string& email);
	User() = default;


	const std::string& getName() const;
	void setName(const std::string& name);

	const std::string& getPassword() const;
	void setPassword(const std::string& password);

	const std::string& getEmail() const;
	void setEmail(const std::string& email);



	bool operator==(const User& other) const;
	bool operator==(const std::string& name) const;
	friend std::ostream& operator<<(std::ostream& str, const User& other);

private:
	std::string m_name;
	std::string m_password;
	std::string m_email;

};