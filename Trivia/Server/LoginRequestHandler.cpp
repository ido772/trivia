// LoginRequestHandler.cpp
//

#include "LoginRequestHandler.h"
#include "RequestHandlerFactory.h"

LoginRequestHandler::LoginRequestHandler(RequestHandlerFactory* requestHandlerFactory)
	: m_user("")
{
	this->m_handlerFactory = requestHandlerFactory;
}

LoginRequestHandler::~LoginRequestHandler()
{
	delete this->m_handlerFactory;
}

/*
Function to check if a request is relevant.
Input:
	requestInfo - Information about the request to check.
Output: whether or not the request was found relevant.
*/
bool LoginRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
	return requestInfo.id == packetRequestCodes::LOGIN_REQUEST || packetRequestCodes::SIGNUP_REQUEST;
}

/*
Function to handle a login or a signup request.
Input:
	requestInfo - Information about the login or singup request.
Output: The result of handling the request.
*/
RequestResult LoginRequestHandler::handleRequest(RequestInfo requestInfo)
{
	RequestResult result;

	try
	{
		switch (requestInfo.id)
		{
		case packetRequestCodes::SIGNUP_REQUEST:
		{
			result = signUp(requestInfo);
			result.newHandler = this;
			break;
		}
		case packetRequestCodes::LOGIN_REQUEST:
		{
			result = login(requestInfo);
			result.newHandler = new MenuRequestHandler(m_user, m_handlerFactory);
			break;
		}
		default:
			throw std::exception("Invalid message code");
		}
	}
	catch (std::exception e)
	{
		/* Create an error response describing what happened. */
		ErrorResponse error = { e.what() };
		result.newHandler = nullptr;
		result.response = JsonResponsePacketSerializer::serializeResponse(error);
	}
	catch (...)
	{
		ErrorResponse error = { "Unknown error" };
		result.newHandler = nullptr;
		result.response = JsonResponsePacketSerializer::serializeResponse(error);
	}

	return result;
}

/*
Function to handle a login request.
Input:
	info - The request info.
Output: A request result.
*/
RequestResult LoginRequestHandler::login(RequestInfo info)
{
	RequestResult result;

	LoginRequest request;
	request = JsonRequestPacketDeserializer::deserializeLoginRequest(info.buffer);
	m_user = { request.username };

	// log the user in.
	m_handlerFactory->getLoginManager()->login(request.username, request.password);

	LoginResponse response = { 1 };
	result.response = JsonResponsePacketSerializer::serializeResponse(response);

	return result;
}

/*
Function to handle a signup request.
Input:
	info - The request info.
Output: A request result.
*/
RequestResult LoginRequestHandler::signUp(RequestInfo info)
{
	RequestResult result;

	SignupRequest request;
	request = JsonRequestPacketDeserializer::deserializeSignupRequest(info.buffer);

	// sign the user up.
	m_handlerFactory->getLoginManager()->signup(request.username, request.password, request.email);

	SignupResponse response = { 2 };
	result.response = JsonResponsePacketSerializer::serializeResponse(response);

	return result;
}

