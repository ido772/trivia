// JsonRequestPacketDeserializer.h
//

#pragma once
#include "nlohmann/json.hpp"
#include "Helper.h"
#include "Requests.h"

class JsonRequestPacketDeserializer
{
public:
	static LoginRequest deserializeLoginRequest(std::string buffer);
	static SignupRequest deserializeSignupRequest(std::string buffer);
	static GetPlayersInRoomRequest deserializeGetPlayersRequest(std::string buffer);
	static JoinRoomRequest deserializeJoinRoomRequest(std::string buffer);
	static CreateRoomRequest deserializeCreateRoomRequest(std::string buffer);
	static SubmitAnswerRequest deserializeSubmitAnswerRequest(std::string buffer);

};

