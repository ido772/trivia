#pragma once
#include <string>

class LoggedUser
{
public:
	LoggedUser(const std::string username);
	~LoggedUser() = default;

	bool operator<(const LoggedUser& rhs) const;

	std::string getUsername();

private:
	std::string m_username;
};