#pragma once
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "Game.h"
#include "GameManager.h"

class RequestHandlerFactory;

class GameRequestHandler: public IRequestHandler
{
public:
	GameRequestHandler(RequestHandlerFactory* requestHandlerFactory, Game game, LoggedUser user);
	GameRequestHandler() = default;
	~GameRequestHandler() = default; 

	virtual bool isRequestRelevant(RequestInfo requestInfo) override;
	virtual RequestResult handleRequest(RequestInfo requestInfo) override;



private:
	Game m_game;
	LoggedUser m_user;
	RequestHandlerFactory* m_handlerFactory;

	RequestResult getQuestion(RequestInfo reqInfo);
	RequestResult submitAnswer(RequestInfo reqInfo);
	RequestResult getGameResults(RequestInfo reqInfo);
	RequestResult leaveGame(RequestInfo reqInfo);

};
