// LoginRequestHandler.h
//

#pragma once
#include "IRequestHandler.h"

class RequestHandlerFactory;

class LoginRequestHandler : public IRequestHandler
{
public:
	LoginRequestHandler(RequestHandlerFactory* requestHandlerFactory);
	LoginRequestHandler() = default;
	~LoginRequestHandler();

	virtual bool isRequestRelevant(RequestInfo requestInfo) override;
	virtual RequestResult handleRequest(RequestInfo requestInfo) override;

private:
	RequestHandlerFactory* m_handlerFactory;
	RequestResult login(RequestInfo info);
	RequestResult signUp(RequestInfo info);

	LoggedUser m_user;
};

