#include "RoomManager.h"

RoomManager::RoomManager()
	: m_rooms()
{
}

/*
This method creates a new room and adds its first logged user
Input: User - logged user to be added to the room
Output: none
*/
void RoomManager::createRoom(LoggedUser user)
{
	RoomData metaData;
	unsigned int max = 0;
	for (auto room : m_rooms)
		max = room.first > max ? room.first : max;
	metaData.id = max + 1;
	metaData.isActive = 1;
	m_rooms[max + 1] = { metaData };
}


/*
This method creates a new room and adds its first logged user
Input: User - logged user to be added to the room
Output: none
*/
void RoomManager::createRoom(LoggedUser user, RoomData& metaData)
{
	unsigned int max = 0;
	for (auto room : m_rooms)
		max = room.first > max ? room.first : max;
	metaData.id = max + 1;
	metaData.isActive = ACTIVE;
	m_rooms[max + 1] = { metaData };
	m_rooms[max + 1].addUser(user);
}

/*
This method deletes the room
Input: ID - the id of the room to be deleted
Output: none
*/
void RoomManager::deleteRoom(int ID)
{
	m_rooms.erase(m_rooms.find(ID));
}

/*
The method gets the room state
Input: id - id of room to be searched for its state
Output: the specific room's state
*/
unsigned int RoomManager::getRoomState(int ID)
{
	if (m_rooms.find(ID) != m_rooms.end()) {
		return m_rooms[ID].getRoomState();
	}
	else {
		throw std::exception("Room is no longer available");
	}
}


/*
This method gets all of the rooms
Input: none
Output: vector of all the rooms
*/
std::vector<Room> RoomManager::getRooms()
{
	std::vector<Room> rooms;
	for (auto room : m_rooms)
		rooms.push_back(room.second);
	return rooms;
}


void RoomManager::delUser(int ID, LoggedUser user)
{
	if (m_rooms.find(ID) != m_rooms.end()) {
		m_rooms[ID].removeUser(user);
	}

	// When the last player leaves, automatically close the room.
	if (m_rooms[ID].getAllUsers().size() == 0) {
		deleteRoom(ID);
	}
}

void RoomManager::delUser(LoggedUser user)
{
	for (auto& i : m_rooms)
	{
		for (auto user : i.second.getAllUsers())
		{
			if (user.getUsername() == user.getUsername())
			{
				delUser(i.second.getRoomData().id, user);
				return;
			}	
		}
	}
}


void RoomManager::addUser(int ID, LoggedUser user)
{
	if (m_rooms.find(ID) != m_rooms.end()) {
		if (m_rooms[ID].getRoomData().maxPlayers > m_rooms[ID].getAllUsers().size()) {
			m_rooms[ID].addUser(user);
		}
		else {
			throw std::exception("Rooms is full");
		}
	}
}

void RoomManager::startGame(int ID)
{
	if (m_rooms.find(ID) != m_rooms.end()) {
		m_rooms[ID].startGame();
	}
}


std::vector<std::string> RoomManager::getUsers(int ID)
{
	std::vector<std::string> usersNames;
	try
	{
		std::vector<LoggedUser> users = m_rooms[ID].getAllUsers();
		for (auto user : users) 
			usersNames.emplace_back(user.getUsername());
	}
	catch (...) {}
	return usersNames;
}