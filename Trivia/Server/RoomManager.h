#pragma once

#include <map>
#include <vector>
#include "Room.h"
#include "LoggedUser.h"

class RoomManager
{
public:
	RoomManager();
	~RoomManager() = default;

	void createRoom(LoggedUser user);
	void createRoom(LoggedUser user, RoomData& metaData);
	void deleteRoom(int ID);
	unsigned int getRoomState(int ID);
	void delUser(int ID, LoggedUser user);
	void delUser(LoggedUser user);
	void addUser(int ID, LoggedUser user);
	std::vector<Room> getRooms();
	void startGame(int ID);
	std::vector<std::string> getUsers(int ID);

private:
	std::map<int, Room> m_rooms;
};
