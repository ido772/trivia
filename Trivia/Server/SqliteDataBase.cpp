#include "SqliteDataBase.h"
#include <io.h>
#include <ostream>
#include <list>
#pragma warning(disable:4996)

#define SQL_STATEMENT_MAX_SIZE 1000

#define ID_FIELD "ID"
#define NAME_FIELD "NAME"
#define PASSWORD_FIELD "PASSWORD"
#define EMAIL_FIELD "EMAIL"

#define QUESTION_FIELD "QUESTION"
#define CORRECT_ANSWER_FIELD "CORRECT_ANSWER"
#define WRONG_ANSWER_1_FIELD "WRONG_ANSWER_1"
#define WRONG_ANSWER_2_FIELD "WRONG_ANSWER_2"
#define WRONG_ANSWER_3_FIELD "WRONG_ANSWER_3"

#define USERNAME_FIELD "USER_NAME"
#define AVG_ANS_TIME_FIELD "AVERAGE_ANSWER_TIME"
#define NUM_CORRECT_ANS_FIELD "CORRECT_ANSWERS"
#define NUM_ANS_FIELD "TOTAL_ANSWERS"
#define NUM_GAMES_FIELD "NUM_OF_GAMES"

#define DEFAULT_QUESTIONS_AMOUNT 10

/*
	Default Constructor 
*/
SqliteDataBase::SqliteDataBase()
{
	if (DATA_BASE_NOT_EXISTS != _access(DATA_BASE_NAME, 0))
	{
		initDB();
	}
	open();
}


/*
	Default Destructor 
closes the data base
*/
SqliteDataBase::~SqliteDataBase()
{
	close();
}


/*
This method opens the data base
Input: none
Output: none
*/
bool SqliteDataBase::open()
{
	if (SQLITE_OK != sqlite3_open(DATA_BASE_NAME, &_db))
	{
		std::cerr << "Failed to open the DataBase" << std::endl;
		exit(EXIT_FAILURE);
	}
}


/*
This method closes the data base
Input: none
Output: none
*/
void SqliteDataBase::close()
{
	sqlite3_close(_db);
	_db = nullptr;
}


/*
This method execute the sql command on the current connected data base
Input: sqlStatement - the sql statement to execute
	   errMsg - char pointer to save there the error message(if there is)
	   callBack - helper function to use in order to save the data from that the statement returns(if you don't intend to do it just write NULL)
	   data - saves the data that the statement returns
Output: none
*/
void SqliteDataBase::executeStatement(const char* statement, char* errMsg, int(*callBack)(void*, int, char**, char**), void* data)
{
	if (SQLITE_OK != sqlite3_exec(_db, statement, callBack, data, &errMsg))
	{
		std::cerr << "Error with statement: " << statement << std::endl;
	}
}


/*
This method inits the data base. It creates the tables of the data base
Input: none
Output: none
*/
void SqliteDataBase::initDB()
{
	char* errMsg = nullptr;
	const char* createUsers = "CREATE TABLE Users( "
											"NAME TEXT, "
											"PASSWORD TEXT, "
											"EMAIL TEXT, "
											"PRIMARY KEY(NAME) "
											");";
	const char* createQuestions = "CREATE TABLE Questions( "
											"QUESTION TEXT, "
											"CORRECT_ANSWER TEXT, "
											"WRONG_ANSWER_1 TEXT, "
											"WRONG_ANSWER_2 TEXT, "
											"WRONG_ANSWER_3 TEXT, "
											"PRIMARY KEY(QUESTION) "
											");";
	const char* createStatistics = "CREATE TABLE Statistics( "
											"USER_NAME TEXT, "
											"AVERAGE_ANSWER_TIME FLOAT, "
											"CORRECT_ANSWERS INTEGER, "
											"TOTAL_ANSWERS INTEGER, "
											"NUM_OF_GAMES INTEGER, "
											"PRIMARY KEY(USER_NAME), "
											"FOREIGN KEY(USER_NAME) REFERENCES Users(NAME)"
											");";
	open();
	try
	{
		executeStatement(createUsers, errMsg, nullptr, nullptr);
		executeStatement(createQuestions, errMsg, nullptr, nullptr);
		executeStatement(createStatistics, errMsg, nullptr, nullptr);
		addDefaultQuestions();
	}
	catch (...)
	{
		close();
		exit(EXIT_FAILURE);
	}
	close();
}


/*
This method callback and receive the users into the data base
*/
int SqliteDataBase::callbackUsers(void* data, int argc, char** argv, char** azColName)
{
	User user;
	std::list<User>* list = (std::list<User>*)data;
	for (int i = 0; i < argc; i++)
	{
		if (NAME_FIELD == std::string(azColName[i]))
		{
			user.setName(argv[i]);
		}
		else if (PASSWORD_FIELD == std::string(azColName[i]))
		{
			user.setPassword(argv[i]);
		}
		else if (EMAIL_FIELD == std::string(azColName[i]))
		{
			user.setEmail(argv[i]);
		}
	}
	list->push_back(user);
	return 0;
}


/*
This method callback and receive the questions into the data base
*/
int SqliteDataBase::callbackQuestions(void* data, int argc, char** argv, char** azColName)
{
	QuestionDB question;
	std::vector<QuestionDB>* vector = (std::vector<QuestionDB>*)data;
	for (int i = 0; i < argc; i++)
	{
		if (QUESTION_FIELD == std::string(azColName[i]))
		{
			question.question = argv[i];
		}
		else if (CORRECT_ANSWER_FIELD == std::string(azColName[i]))
		{
			question.correctAnswer = argv[i];
		}
		else if (WRONG_ANSWER_1_FIELD == std::string(azColName[i]))
		{
			question.wrong_answers[0] = argv[i];
		}
		else if (WRONG_ANSWER_2_FIELD == std::string(azColName[i]))
		{
			question.wrong_answers[1] = argv[i];
		}
		else if (WRONG_ANSWER_3_FIELD == std::string(azColName[i]))
		{
			question.wrong_answers[2] = argv[i];
		}
	}
	vector->push_back(question);
	return 0;
}


/*
This method callback and receive the user statistics into the data base
*/
int SqliteDataBase::callbackUserStatistics(void* data, int argc, char** argv, char** azColName)
{
	userStatistics userInfo;
	std::vector<userStatistics>* vector = (std::vector<userStatistics>*)data;
	for (int i = 0; i < argc; i++)
	{
		if (USERNAME_FIELD == std::string(azColName[i]))
		{
			userInfo.userName = argv[i];
		}
		else if (AVG_ANS_TIME_FIELD == std::string(azColName[i]))
		{
			userInfo.averageAnswerTime = std::stof(argv[i]);
		}
		else if (NUM_CORRECT_ANS_FIELD == std::string(azColName[i]))
		{
			userInfo.correctAnswers = std::stoi(argv[i]);
		}
		else if (NUM_ANS_FIELD == std::string(azColName[i]))
		{
			userInfo.totalAnswers = std::stoi(argv[i]);
		}
		else if (NUM_GAMES_FIELD == std::string(azColName[i]))
		{
			userInfo.numOfGames = std::stoi(argv[i]);
		}
		else {
			std::cout << "Invalid col: " << azColName[i] << " - " << argv[i] << std::endl;
		}
	}
	vector->push_back(userInfo);
	return 0;
}

/*
This method creates a user and add it into the data base
Input: user - user to be created
Output: none
*/
void SqliteDataBase::createUser(User& user)
{
	char statement[SQL_STATEMENT_MAX_SIZE] = "";
	sprintf(statement, "INSERT INTO USERS (NAME, PASSWORD, EMAIL) VALUES (\"%s\",\"%s\",\"%s\");", user.getName().c_str(), user.getPassword().c_str(), user.getEmail().c_str());
	char* errMsg = nullptr;
	executeStatement(statement, errMsg, nullptr, nullptr);
}


/*
This function checks if a user exists
Input: userName - the user name to be searched
Output: true if the user exist otherwise false
*/
bool SqliteDataBase::doesUserExists(const std::string& userName)
{
	char statement[SQL_STATEMENT_MAX_SIZE] = "";
	std::list<User> users;
	char* errMsg = nullptr;
	sprintf(statement, "SELECT * FROM USERS WHERE NAME = \"%s\";", userName.c_str());
	executeStatement(statement, errMsg, SqliteDataBase::callbackUsers, (void*)&users);
	return !users.empty();
}


/*
This function checks if a user exists and if the password is correct
Input: userName - the user name to be searched
	   password - password to check if its of user
Output: true if the user exist and the password is correct otherwise false
*/
bool SqliteDataBase::doesPasswordMatch(const std::string& userName, const std::string& password)
{
	char statement[SQL_STATEMENT_MAX_SIZE] = "";
	std::list<User> users;
	char* errMsg = nullptr;
	sprintf(statement, "SELECT * FROM USERS WHERE NAME = \"%s\" AND PASSWORD = \"%s\";", userName.c_str(), password.c_str());
	executeStatement(statement, errMsg, SqliteDataBase::callbackUsers, (void*)&users);
	return !users.empty();
}


/*
This method adds default questions to the data base of the trivia
Input: none
Output: none
*/
void SqliteDataBase::addDefaultQuestions()
{
	QuestionDB questions[] = {
							{"PROTOTYPE_QUESTION", "PROTOTYPE_CORRECT", {"PROTOTYPE_WRONG_1", "PROTOTYPE_WRONG_2", "PROTOTYPE_WRONG_3"}},
							{"What is the color of the sky?", "Blue", {"Yellow", "Red", "Pink"}},
							{"What is the best name for a boy?", "Ido", {"Lior", "Tal", "Dima"}},
							{"What is the best name for a girl?", "Ido", {"Lior", "Tal", "Dima"}},
							{"What is the current year?", "2020", {"2021", "2019", "2022"}},
							{"What is the best city in Israel?", "Hadera", {"Haifa", "Tel-Aviv", "Eilat"}},
							{"What is the name of the best instructor", "Ido", {"Andrey", "Yotam", "Lior"}},
							{"What is the best operation system?", "Linux", {"Windows", "Hana Montana OS", "MAC OS"}},
							{"What is the best programing language ever?", "C++", {"C", "Java", "C#"}},
							{"What is fun?", "Sleeping", {"Studing", "Fighting", "Watching TV"}},
							{"Where did Ron go to after the bomb went off?", "Everywhere", {"Emergency room", "Home", "To eat ice cream"}}
	};
	char statement[SQL_STATEMENT_MAX_SIZE] = "";
	char* errMsg = nullptr;
	
	for (int i = 0; i < DEFAULT_QUESTIONS_AMOUNT + 1; i++)
	{
		std::stringstream statemen;
		statemen << "INSERT INTO Questions ";
		statemen << "(QUESTION, CORRECT_ANSWER, WRONG_ANSWER_1, WRONG_ANSWER_2, WRONG_ANSWER_3) ";
		statemen << "VALUES (\"" << questions[i].question << "\","
			<< "\"" << questions[i].correctAnswer << "\","
			<< "\"" << questions[i].wrong_answers[0] << "\","
			<< "\"" << questions[i].wrong_answers[1] << "\","
			<< "\"" << questions[i].wrong_answers[2] << "\");";
		executeStatement(statemen.str().c_str(), errMsg, nullptr, nullptr);
	}
}


/*
This function gets a certain amount of questions from the data base
Input: questionsAmoutn
Output: vector of questions
*/
std::vector<QuestionDB> SqliteDataBase::getQuestions(int questionsAmount)
{ 
	char statement[SQL_STATEMENT_MAX_SIZE] = "";
	std::vector<QuestionDB> questions;
	char* errMsg = nullptr;
	sprintf(statement, "SELECT * FROM QUESTIONS LIMIT %d;", questionsAmount);
	executeStatement(statement, errMsg, SqliteDataBase::callbackQuestions, (void*)&questions);
	return questions; 
}

float SqliteDataBase::getPlayerAverageAnswerTime(std::string userName) 
{ 
	char statement[SQL_STATEMENT_MAX_SIZE] = "";
	std::vector<userStatistics> userStatistic;
	char* errMsg = nullptr;
	sprintf(statement, "SELECT * FROM Statistics WHERE USER_NAME = \"%s\" ;", userName);
	executeStatement(statement, errMsg, SqliteDataBase::callbackUserStatistics, (void*)&userStatistic);
	return userStatistic[0].averageAnswerTime;
}

int SqliteDataBase::getNumOfCorrectAnswers(std::string userName)
{
	char statement[SQL_STATEMENT_MAX_SIZE] = "";
	std::vector<userStatistics> userStatistic;
	char* errMsg = nullptr;
	sprintf(statement, "SELECT * FROM Statistics WHERE USER_NAME = \"%s\" ;", userName);
	executeStatement(statement, errMsg, SqliteDataBase::callbackUserStatistics, (void*)&userStatistic);
	return userStatistic[0].correctAnswers;
}

int SqliteDataBase::getNumOfTotalAnswers(std::string userName) 
{ 
	char statement[SQL_STATEMENT_MAX_SIZE] = "";
	std::vector<userStatistics> userStatistic;
	char* errMsg = nullptr;
	sprintf(statement, "SELECT * FROM Statistics WHERE USER_NAME = \"%s\" ;", userName);
	executeStatement(statement, errMsg, SqliteDataBase::callbackUserStatistics, (void*)&userStatistic);
	return userStatistic[0].totalAnswers;
}

int SqliteDataBase::getNumOfPlayerGames(std::string userName)
{
	char statement[SQL_STATEMENT_MAX_SIZE] = "";
	std::vector<userStatistics> userStatistic;
	char* errMsg = nullptr;
	sprintf(statement, "SELECT * FROM Statistics WHERE USER_NAME = \"%s\" ;", userName);
	executeStatement(statement, errMsg, SqliteDataBase::callbackUserStatistics, (void*)&userStatistic);
	return userStatistic[0].numOfGames;
}

std::vector<userStatistics> SqliteDataBase::getTop5Players()
{
	char statement[SQL_STATEMENT_MAX_SIZE] = "SELECT * FROM Statistics ORDER BY CORRECT_ANSWERS DESC LIMIT 5;";
	std::vector<userStatistics> userStatistics;
	char* errMsg = nullptr;
	executeStatement(statement, errMsg, &SqliteDataBase::callbackUserStatistics, (void*)&userStatistics);
	return userStatistics;
}