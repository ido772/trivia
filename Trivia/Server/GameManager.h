#pragma once
#include "IDatabase.h"
#include "Game.h"
#include "Room.h"
#include "Responses.h"

class GameManager
{
public:
	GameManager(IDatabase* database);
    ~GameManager() = default;

	Question getQuestionForUser(LoggedUser user);
	void createRoom(Room room);
	void submitAnswer(LoggedUser, int ans);
	std::list<PlayerResults> getResults(LoggedUser user);
	void deleteGame(Room room) {};
	Game* getGame(LoggedUser user);

private:
	IDatabase* m_database;
	std::vector<Game> m_games;
};