#include "RoomMemberRequestHandler.h"
#include "MenuRequestHandler.h"


RoomMemberRequestHandler::RoomMemberRequestHandler(RequestHandlerFactory* handlerFactory, LoggedUser user, Room room)
	: m_user(user), m_handlerFactory(handlerFactory), m_room(room)
{

}

RequestResult RoomMemberRequestHandler::handleRequest(RequestInfo requestInfo) 
{ 
	RequestResult result;

	try
	{
		switch (requestInfo.id)
		{
			case packetRequestCodes::LEAVE_ROOM_REQUEST:
			{
				result = leaveRoom(requestInfo);
				result.newHandler = new MenuRequestHandler(m_user, m_handlerFactory);
				break;
			}
			case packetRequestCodes::GET_ROOM_STATE_REQUEST:
			{
				result = getRoomState(requestInfo);
				break;
			}
			default:
				throw std::exception("Invalid message code");
			}
	}
	catch (std::exception e)
	{
		/* Create an error response describing what happened. */
		ErrorResponse error = { e.what() };
		result.newHandler = new MenuRequestHandler(m_user, m_handlerFactory);
		result.response = JsonResponsePacketSerializer::serializeResponse(error);
	}
	catch (...)
	{
		ErrorResponse error = { "Unknown error" };
		result.newHandler = nullptr;
		result.response = JsonResponsePacketSerializer::serializeResponse(error);
	}

	return result;
}


RequestResult RoomMemberRequestHandler::getRoomState(RequestInfo requestInfo)
{
	int roomState = m_handlerFactory->getRoomManager()->getRoomState(m_room.getRoomData().id);

	RequestResult result;
	GetRoomStateResponse response = { roomState, STARTED == roomState, m_handlerFactory->getRoomManager()->getUsers(m_room.getRoomData().id) , 0, 0 };
	result.response = JsonResponsePacketSerializer::serializeResponse(response);
	if (NOT_ACTIVE == roomState)
	{
		result.newHandler = new MenuRequestHandler(m_user, m_handlerFactory);
	}
	else if (ACTIVE == roomState)
	{
		result.newHandler = this;
	}
	else if (STARTED == roomState)
	{
		result.newHandler = m_handlerFactory->createGameRequestHandler(m_user);
	}
	else {
		result.newHandler = nullptr;
	}
	return result;
} 


RequestResult RoomMemberRequestHandler::leaveRoom(RequestInfo requestInfo)
{
	m_handlerFactory->getRoomManager()->delUser(m_room.getRoomData().id, m_user);

	RequestResult result;
	LeaveRoomResponse response = { 1 };
	result.response = JsonResponsePacketSerializer::serializeResponse(response);
	return result;
}

bool RoomMemberRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{ 
	return (requestInfo.id == packetRequestCodes::GET_ROOM_STATE_REQUEST)
		|| (requestInfo.id == packetRequestCodes::LEAVE_ROOM_REQUEST);
}


