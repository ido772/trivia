// JsonRequestPacketDeserializer.cpp
//

#include "JsonRequestPacketDeserializer.h"

/*
Function to deserialize a login request buffer.
Input:
	buffer - The string buffer of the login request.
Output: A LoginRequest struct representing the login request.
*/
LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(std::string buffer)
{
	/* Check the validation of the buffer. */
	if (!nlohmann::json::accept(buffer) 
		|| !Helper::doesContain(buffer, "username")
		|| !Helper::doesContain(buffer, "password"))
	{
		throw std::exception("Invalid login request");
	}

	nlohmann::json deserializedBuffer = nlohmann::json::parse(buffer);

	LoginRequest request = {
		deserializedBuffer["username"],
		deserializedBuffer["password"]
	};
	return request;
}

/*
Function to deserialize a signup request buffer.
Input:
	buffer - The string buffer of the signup request.
Output: A SignupRequest struct representing the signup request.
*/
SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(std::string buffer)
{
	/* Check the validation of the buffer. */
	if (!nlohmann::json::accept(buffer)
		|| !Helper::doesContain(buffer, "username")
		|| !Helper::doesContain(buffer, "password")
		|| !Helper::doesContain(buffer, "email"))
	{
		throw std::exception("Invalid signup request");
	}

	nlohmann::json deserializedBuffer = nlohmann::json::parse(buffer);

	SignupRequest request = {
		deserializedBuffer["username"],
		deserializedBuffer["password"],
		deserializedBuffer["email"]
	};
	return request;
}

/*
Function to deserialize a get-players request buffer.
Input:
	buffer - The string buffer of the get-players request.
Output: A GetPlayersInRoomRequest struct representing the get-players request.
*/
GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersRequest(std::string buffer)
{
	/* Check the validation of the buffer. */
	if (!nlohmann::json::accept(buffer)
		|| !Helper::doesContain(buffer, "roomId"))
	{
		throw std::exception("Invalid get-players request");
	}

	nlohmann::json deserializedBuffer = nlohmann::json::parse(buffer);

	GetPlayersInRoomRequest request = {
		deserializedBuffer["roomId"]
	};
	return request;
}

/*
Function to deserialize a join-room request buffer.
Input:
	buffer - The string buffer of the join-room request.
Output: A JoinRoomRequest struct representing the join-room request.
*/
JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(std::string buffer)
{
	/* Check the validation of the buffer. */
	if (!nlohmann::json::accept(buffer)
		|| !Helper::doesContain(buffer, "roomId"))
	{
		throw std::exception("Invalid join-room request");
	}

	nlohmann::json deserializedBuffer = nlohmann::json::parse(buffer);

	JoinRoomRequest request = {
		deserializedBuffer["roomId"]
	};
	return request;
}

/*
Function to deserialize a create-room request buffer.
Input:
	buffer - The string buffer of the create-room request.
Output: A CreateRoomRequest struct representing the create-room request.
*/
CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(std::string buffer)
{
	/* Check the validation of the buffer. */
	if (!nlohmann::json::accept(buffer)
		|| !Helper::doesContain(buffer, "name")
		|| !Helper::doesContain(buffer, "maxPlayers")
		|| !Helper::doesContain(buffer, "questionsCount")
		|| !Helper::doesContain(buffer, "answerTimeout"))
	{
		throw std::exception("Invalid create-room request");
	}

	nlohmann::json deserializedBuffer = nlohmann::json::parse(buffer);

	CreateRoomRequest request = {
		deserializedBuffer["name"],
		deserializedBuffer["maxPlayers"],
		deserializedBuffer["questionsCount"],
		deserializedBuffer["answerTimeout"]
	};
	return request;
}

SubmitAnswerRequest JsonRequestPacketDeserializer::deserializeSubmitAnswerRequest(std::string buffer)
{
	/* Check the validation of the buffer. */
	if (!nlohmann::json::accept(buffer)
		|| !Helper::doesContain(buffer, "answerId"))
	{
		throw std::exception("Invalid submit-answer request");
	}

	nlohmann::json deserializedBuffer = nlohmann::json::parse(buffer);

	SubmitAnswerRequest request = {
		deserializedBuffer["answerId"]
	};
	return request;
}
