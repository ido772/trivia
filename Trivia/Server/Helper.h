// Helper.h
//

#pragma once
#pragma comment(lib, "Ws2_32.lib")
#include <WinSock2.h>
#include <string>
#include <sstream>
#include <iomanip>

#define DEFAULT_BUFFER_LEN 1024

class Helper
{
public:
	static std::string getMessageFromSocket(SOCKET sc);
	static int getIntPartFromSocket(SOCKET sc, int bytesNum);
	static std::string getStringPartFromSocket(SOCKET sc, int bytesNum);
	static void sendData(SOCKET sc, std::string message);

	static std::string getPaddedNumber(int num, int digits);

	static bool doesContain(const std::string& str, const std::string& substr);

private:
	static char* getDataFromSocket(SOCKET sc);
	static char* getPartFromSocket(SOCKET sc, int bytesNum);
	static char* getPartFromSocket(SOCKET sc, int bytesNum, int flags);

};

