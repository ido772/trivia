#include "Game.h"

Game::Game(std::vector<Question> questions, std::vector<LoggedUser> users)
{
	m_questions = questions;
	GameData gameData = { m_questions[0] , 0, 0, 0 };
	for (auto user : users)
	{
		m_players[user] = gameData;
	}
}

Question Game::getQuestionForUser(LoggedUser user)
{
	bool flag = false;
	Question question;
	for (auto i : m_questions)
	{
		if (flag)
		{
			question = i;
			m_players[user].currentQuestion = question;
			m_players[user].lastAnswerTime = std::chrono::system_clock::now();
			break;
		}
		if (i.getQuestion() == m_players[user].currentQuestion.getQuestion())
		{
			flag = true;
		}
	}
	return question;
}

void Game::removePlayer(LoggedUser user)
{
	m_players.erase(user);
}

unsigned int Game::isGameOver()
{
	for (auto player : m_players)
	{
		// Check if the user have answered all of the questions.
		if (10 != player.second.correctAnswerCount + player.second.wrongAnswerCount)
		{
			return 0;
		}
	}

	return 1;
}


void Game::submitAnswer(LoggedUser user, int ans)
{
	// Calcuate the answer time.
	std::chrono::system_clock::time_point submitTime = std::chrono::system_clock::now();
	std::chrono::duration<float> answerTime = submitTime - m_players[user].lastAnswerTime;

	// Update the average answer time.
	long numOfAnswers = static_cast<long>(m_players[user].correctAnswerCount + m_players[user].wrongAnswerCount);
	this->m_players[user].averageAnswerTime *= numOfAnswers++;
	m_players[user].averageAnswerTime += answerTime.count();
	m_players[user].averageAnswerTime /= numOfAnswers;

	// Update answer counters.
	m_players[user].correctAnswerCount += (1 == ans ? 1 : 0);
	m_players[user].wrongAnswerCount += (1 != ans ? 1 : 0);
}



