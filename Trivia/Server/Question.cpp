#include "Question.h"

Question::Question(std::string question, std::vector<std::string> possibleAnswers) : m_question(question), m_possibleAnswers(possibleAnswers)
{
}

std::string Question::getQuestion()
{
	return m_question;
}

std::vector<std::string> Question::getPossibleAnswers()
{
	return m_possibleAnswers;
}

std::string Question::getCorrectAnswer()
{
	return *m_possibleAnswers.begin();
}

Question Question::convert_struct_to_question(QuestionDB question)
{
	std::vector<std::string> possibleAnswers;
	possibleAnswers.emplace_back(question.correctAnswer);
	possibleAnswers.emplace_back(question.wrong_answers[0]);
	possibleAnswers.emplace_back(question.wrong_answers[0]);
	possibleAnswers.emplace_back(question.wrong_answers[1]);

	return Question(question.question, possibleAnswers);
}

std::vector<Question> Question::convert_vector_struct_to_question(std::vector<QuestionDB> questionsStruct)
{
	std::vector<Question> questions;
	for (auto question : questionsStruct)
	{
		questions.emplace_back(convert_struct_to_question(question));
	}
	return questions;
}