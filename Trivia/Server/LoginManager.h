#pragma once
#include "IDatabase.h"
#include "LoggedUser.h"
#include <vector>

class LoginManager
{
public:
	LoginManager();
	~LoginManager();

	void signup(const std::string& userName, const std::string& password, const std::string& email);
	void login(const std::string& userName, const std::string& password);
	void logout(const std::string& userName);

private:
	IDatabase* m_database;
	std::vector<LoggedUser> m_loggedUsers;
};