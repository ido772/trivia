#pragma once
#include "IDatabase.h"
#include <string>
#include <vector>

class Question
{
public:
	Question(std::string question, std::vector<std::string> possibleAnswers);
	Question() = default;
	~Question() = default;

	std::string getQuestion();
	std::vector<std::string> getPossibleAnswers();
	std::string getCorrectAnswer();

	static Question convert_struct_to_question(QuestionDB question);
	static std::vector<Question> convert_vector_struct_to_question(std::vector<QuestionDB> question);

private:
	std::string m_question;
	std::vector<std::string> m_possibleAnswers;
};

