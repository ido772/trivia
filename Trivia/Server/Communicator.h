// Communicator.cpp
//

#pragma once
#include "Helper.h"
#include <map>
#include <iostream>
#include <string>
#include <thread>
#include <chrono>
#include "LoginRequestHandler.h"
#include "RequestHandlerFactory.h"

#define SERVER_LISTENING_PORT 5555
#define SERVER_BINDING_IP "127.0.0.1"

class Communicator
{
public:
	Communicator();
	~Communicator();

	// Methods
	void startHandleRequests();

private:
	RequestHandlerFactory* m_requestHandlerFactory;

	// Properties
	std::map<SOCKET, IRequestHandler*> m_clients;
	SOCKET m_listenerSocket;

	// Private methods
	void bindAndListen();
	void handleNewClient(SOCKET clientSocket);

};

