// Communicator.cpp
//

#include "Communicator.h"

/*
	Empty Constructor
*/
Communicator::Communicator()
	: m_clients(), m_listenerSocket()
{	
	m_requestHandlerFactory = new RequestHandlerFactory();
}

/*
	Destructor
*/
Communicator::~Communicator()
{
	try
	{
		closesocket(m_listenerSocket);
	}
	catch (...) {}

	/* Close all connection to the clients. */
	for (auto client : m_clients) 
	{
		try
		{
			closesocket(client.first);
		}
		catch (...) {}
		delete(client.second);
	}
}

/*
Function to start handling client requests.
Input: None.
Output: None.
*/
void Communicator::startHandleRequests()
{
	bindAndListen();

	while (true)
	{
		try
		{
			/* Accept a new client. */
			SOCKET clientSocket = ::accept(m_listenerSocket, NULL, NULL);
			if (clientSocket == INVALID_SOCKET)
			{
				throw std::exception(__FUNCTION__ " - Failed to accept new client");
			}

			// Add the client to the clients map.
			m_clients[clientSocket] = new LoginRequestHandler(m_requestHandlerFactory);
			std::cout << "New client accepted. Server and client can speak" << std::endl;

			// Open a new thread to handle the client.
			std::thread clientThread(&Communicator::handleNewClient, this, clientSocket);
			clientThread.detach();

		}
		catch (std::exception e) {
			std::cout << "Error <" << e.what() << ">" << std::endl;
		}
		catch (...) {}
	}
}

/*
Binding the socket and starting listening 
Input: None.
Output: None.
*/
void Communicator::bindAndListen()
{
	m_listenerSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (INVALID_SOCKET == m_listenerSocket)
	{
		throw std::exception(__FUNCTION__ " - Failed to create listener socket");
	}
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(SERVER_LISTENING_PORT); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(m_listenerSocket, (struct sockaddr*) & sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - Failed to bind listener socket");

	// Start listening for incoming requests of clients
	if (listen(m_listenerSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - Failed to listen");
	std::cout << "Listening on port " << SERVER_LISTENING_PORT << std::endl;
}

/*
This method handle the client that been received
Input: none
Output: none
*/
void Communicator::handleNewClient(SOCKET clientSocket)
{
	try
	{
		do
		{
			// Get the message code.
			int code = *reinterpret_cast<const unsigned char*>(Helper::getStringPartFromSocket(clientSocket, 1).c_str());

			// Get the message content.
			int contentSize = Helper::getIntPartFromSocket(clientSocket, 4);
			std::string content = Helper::getStringPartFromSocket(clientSocket, contentSize);

			// Instantiate a request.
			RequestInfo request;
			request.id = code;
			request.receivalTime = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
			request.buffer = content;

			// Handle the request.
			RequestResult response;

			// Check if the request is relevant.
			if (m_clients[clientSocket]->isRequestRelevant(request)) {
				// Handle the request.
				response = m_clients[clientSocket]->handleRequest(request);

				// Send the response back to the client.
				Helper::sendData(clientSocket, response.response);

				// Set the new request handler.
				m_clients[clientSocket] = response.newHandler;
			}
			else {
				ErrorResponse error = { "Irrelevant request" };
				Helper::sendData(clientSocket, JsonResponsePacketSerializer::serializeResponse(error));
			}
		} while (m_clients[clientSocket] != nullptr);
	}
	catch (const std::exception & e)
	{
		closesocket(clientSocket);
		std::cout << "Error on socket s" << std::to_string(clientSocket) 
			<< " <" << e.what() << ">" << std::endl;
	}
}
