import socket
import configman as cfg


def send_data(sock, message):
    """
    Function to send a message data through a socket.
    :param sock: The socket
    :param message: The message
    :return: None
    """

    buffer = message.encode()
    sock.sendall(buffer)


def recv_data(sock):
    """
    Function to receive a message data through a socket
    :param sock: The socket
    :return: The message
    :rtype: str
    """

    data = sock.recv(1024)
    return data.decode()


def main():
    """
    Main function of the program, simulating a demo client for the Trivia 1.0.1 server.
    :return:
    """

    # print general information about the program
    print('=======================================')
    print('Demo client for the Trivia 1.0.1 server')
    print('Configuration settings:')
    print('\tserver ip address: {}'.format(cfg.config['server_address']))
    print('\tserver port: {}'.format(cfg.config['server_port']))
    print('=======================================')

    # connect to the server
    server_address = (cfg.config['server_address'], cfg.config['server_port'])
    print('Connecting to server at {}'.format(server_address))
    connection = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    connection.connect(server_address)
    print('Connected')

    # # send a signup request
    # signup_request_content = '{"username": "idos", "password": "idohagever123", "email": "idos@mail.com"}'
    # signup_request = chr(10) + str(len(signup_request_content)).zfill(4) + signup_request_content
    # print('Sending signup request: ')
    # print(signup_request)
    # send_data(connection, signup_request)
    #
    # # receive a server response
    # response = recv_data(connection)
    # print('Response received: {}'.format(response))

    # send a login request
    login_request_content = '{"username": "idos", "password": "idohagever123"}'
    login_request = chr(20) + str(len(login_request_content)).zfill(4) + login_request_content
    print('Sending login request: ')
    print(login_request)
    send_data(connection, login_request)

    # receive a server response
    response = recv_data(connection)
    print('Response received: {}'.format(response))


# __name__ guard
if __name__ == '__main__':
    main()
