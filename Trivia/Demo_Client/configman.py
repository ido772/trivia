# constants
DEFAULT_ADDRESS = '127.0.0.1'
DEFAULT_PORT = 65000
CONFIG_FILENAME = 'config.cfg'

# config dict
config = {}


def get_config_properties():
    """
    Function to get the config properties form the config file into the config dict.
    :return: None
    """
    content = open(CONFIG_FILENAME, 'r')

    # set default values in the config dict
    config['server_address'] = DEFAULT_ADDRESS
    config['server_port'] = DEFAULT_PORT

    # loop through the config file
    for line in content:
        cfg_property = line.split(':')[0]
        value = line.split(':')[1][:-1]

        # set the property in the config dict
        if value.isnumeric():
            config[cfg_property] = int(value)
        else:
            config[cfg_property] = value


get_config_properties()


# __name__ guard
if __name__ == '__main__':
    get_config_properties()
